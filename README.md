# ASTnode

## Getting started

```
yarn
yarn dev
```

## Import in your project

basic example (see App.tsx)
```
  const [tree] = useParser({
    ast,
    meta,
    langId,
    isEmpty,
  });

  const ctx = useContext(SelectionContext);

  return (
    <ASTNode node={tree} level={level} ctx={ctx} />
  )
```

### Props

- `node` the parsed astNode example
-  `isTranspiler` determines if the example is of transpiler type
- `ctx` the application context object, see next chapter
- `titleOverride` replaces the node title with a custom string (optional)
- `viewNode` custom function that get called when the "view node" button is clicked (optional)
- `hasDocs` determines if the node should show the docs links (optional)

### Ctx

In order to make the ASTNode component to work properly the ctx object must contain at least the following attributes:
```
{
  source: source node tree
  target: target node tree
  select: function to be invoked on node selection
}
```
Please take a look at the `SelectionProvider` file in this project in order to understand the logic behind these values