import { EcoreMetamodelSupport } from '@strumenta/tylasu/interop/ecore';
import { TranspilerExample } from './TranspilerExample.type';

export type TranspilerType = {
  name: string;
  srcName: string;
  srcMeta: EcoreMetamodelSupport;
  targetName: string;
  targetMeta: EcoreMetamodelSupport;
  hasDocs: boolean;
  hasMultipleFiles: boolean;
  example: TranspilerExample;
};
