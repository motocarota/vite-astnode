import { ExampleFile } from './ExampleFile.type';

export type TranspilerExample = {
  originalCode?: string;
  generatedCode?: string;
  issues: object;
  // hasMultipleFiles: true
  generatedFiles?: ExampleFile[];
  originalFiles?: ExampleFile[];
  // hasMultipleFiles: false
  sourceResult?: object;
  targetResult?: object;
};
