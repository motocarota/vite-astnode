import { TraceNode } from '@strumenta/tylasu/interop/strumenta-playground';

export type ExampleFile = {
  path: string;
  code: string;
  result: TraceNode;
};
