import { Position } from '@strumenta/tylasu';
import { SourceNode, TargetNode, TraceNode } from '@strumenta/tylasu/interop/strumenta-playground';
import Attribute from './Attribute.type';
import From from './From.type';

interface WrappedNode {
  key: string,
  title: string,
  path: string,
  type: string,
  eClassName: string,
  qualifiedName: string,
  attributes: Attribute[],
  childrens: WrappedNode[],
  position: Position,
  inverseKey: string | null;
  inverseFile: string | null;
  inverseNode: TargetNode | SourceNode | null;
  inversePosition: Position | null;
  original: TraceNode,
  from: From,
  isTranspiler: boolean,
  hasMultipleFiles: boolean,
}

export default WrappedNode;
