import From from './From.type';
import WrappedNode from './WrappedNode.type';

export interface SelectionCtx {
  languages: { [key: string]: string };
  source: WrappedNode | null;
  target: WrappedNode | null;
  sourceTree: WrappedNode | null;
  targetTree: WrappedNode | null;
  files: string[];
  select: (n: {
    node: WrappedNode;
    isTarget: boolean;
    isTranspiler: boolean;
    from: From;
    hasMultipleFiles?: boolean;
  }) => void;
  storeTree: (args: {
    s: WrappedNode | null;
    t: WrappedNode | null;
    isTranspiler: boolean;
  }) => void;
  storeLanguages: (l: { [key: string]: string }) => void;
  setFiles: (n: { name: string; id: string }[]) => void;
}
