import React from "react";
import ReactDOM from "react-dom/client";
import { SelectionProvider } from "./lib/contexts/SelectionProvider";
import App from "./App";

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <SelectionProvider>
      <App />
    </SelectionProvider>
  </React.StrictMode>
);
