import ASTNode from "./lib/ASTNode";
import useParser from "./lib/hooks/useParser";
import { SelectionContext } from "./lib/contexts/SelectionProvider";
import { useContext } from "react";
import meta from "./lib/examples/meta";
import ast from "./lib/examples/ast";

function App() {
  const [tree] = useParser({
    ast,
    meta,
    langId: "java",
    level: 0,
  });

  const ctx = useContext(SelectionContext);

  return (
    <div className="App" style={{ width: 800, margin: "auto" }}>
      <ASTNode node={tree} level={0} ctx={ctx} langId="java" />
    </div>
  );
}

export default App;
