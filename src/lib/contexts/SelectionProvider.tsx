import _forEach from "lodash/forEach";
import { createContext, useCallback, useMemo, useState } from "react";
import getNodeFromPoint from "../utils/getNodeFromPoint";
import isRootNode from "../utils/isRootNode";
import WrappedNode from "../../types/WrappedNode.type";
import From from "../../types/From.type";
import getFields from "../utils/getFields";

export const SelectionContext = createContext({
  languages: {},
  source: null,
  target: null,
  sourceTree: null,
  targetTree: null,
  files: [],
  select: (n) => {
    console.error("[NYI] select by AST", n);
  },
  storeTree: ({ s, t, isTranspiler }) => {
    console.error("[NYI] store tree", s, t, isTranspiler);
  },
  storeLanguages: (t) => {
    console.error("[NYI] store langs", t);
  },
  setFiles: (t) => {
    console.error("[NYI] set files", t);
  },
});

export interface SelectProps {
  node: WrappedNode;
  from: From;
  isTranspiler: boolean;
  isTarget: boolean;
  hasMultipleFiles: boolean;
}

export function SelectionProvider({ children }) {
  const [languages, setLanguages] = useState({});
  const [sourceTree, setSourceTree] = useState(null);
  const [targetTree, setTargetTree] = useState(null);
  const [source, setSource] = useState(null);
  const [target, setTarget] = useState(null);
  const [files, setFiles] = useState([]);

  const select = useCallback(
    ({
      node,
      from, // "AST" or "code"
      isTranspiler = false,
      isTarget,
      hasMultipleFiles = false,
    }: SelectProps): void => {
      // if no node select none
      if (!node) {
        setSource(null);
        setTarget(null);
        return;
      }

      const { key, path } = node;

      // do not select root node
      if (isRootNode(path)) {
        return;
      }

      // do nothing is node is already selected
      const selection = isTarget ? target : source;
      if (selection?.key === key) {
        return;
      }

      // store node
      isTarget ? setTarget(node) : setSource(node);

      // get inverse node
      // eslint-disable-next-line no-nested-ternary
      const t = hasMultipleFiles
        ? node?.inverseNode?.file?.node
        : isTarget
        ? targetTree
        : sourceTree;

      if (hasMultipleFiles) {
        isTarget ? setSourceTree(t) : setTargetTree(t);
      }

      const n =
        node.inverseNode ||
        getNodeFromPoint({
          position: node.inversePosition,
          tree: t,
        });

      const inode = getFields({
        node: n,
        isTranspiler,
        isTarget: !isTarget,
        level: 1,
        hasMultipleFiles,
        from,
      });

      // store inverse node
      if (isTarget) {
        setSource(inode);
      } else {
        setTarget(inode);
      }
    },
    [target, source, targetTree, sourceTree]
  );

  const storeTree = useCallback(({ s, t, isTranspiler }) => {
    if (isTranspiler) {
      // transpiler
      setSourceTree(s);
      setTargetTree(t);
    } else {
      // parser
      setSourceTree(s);
      setTargetTree(null);
    }
  }, []);

  const storeLanguages = useCallback((langs) => {
    const res = {};
    _forEach(langs, (row) => {
      res[row.id] = row.name;
    });
    setLanguages(res);
  }, []);

  // memoize values to skip re-renders
  const value = useMemo(
    () => ({
      source,
      target,
      sourceTree,
      targetTree,
      select,
      storeTree,
      languages,
      storeLanguages,
      files,
      setFiles,
    }),
    [
      source,
      target,
      sourceTree,
      targetTree,
      select,
      storeTree,
      languages,
      storeLanguages,
      files,
      setFiles,
    ]
  );

  return (
    <SelectionContext.Provider value={value}>
      {children}
    </SelectionContext.Provider>
  );
}
