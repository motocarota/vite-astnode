export default {
  eClass: "http://www.eclipse.org/emf/2002/Ecore#//EPackage",
  name: "com.strumenta.extendedsqlparser.ast",
  nsURI: "https://strumenta.com/extendedsql",
  nsPrefix: "extendedsql",
  eClassifiers: [
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "Actions",
      eLiterals: [
        {
          name: "ADD",
        },
        {
          name: "REMOVE",
          value: 1,
        },
        {
          name: "SET",
          value: 2,
        },
        {
          name: "DROP",
          value: 3,
        },
        {
          name: "MODIFY",
          value: 4,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "Expression",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "InquiryDirective",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Expression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "text",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//string",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "NumericNegative",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Expression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "numeric",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DatetimeExpression",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Expression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "VarrayItem",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Expression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "id_expression",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SelectElement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Expression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "element",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "UsingModifier",
      eLiterals: [
        {
          name: "IN",
        },
        {
          name: "IN_OUT",
          value: 1,
        },
        {
          name: "OUT",
          value: 2,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "UsingElement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Expression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "element",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "modifier",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//UsingModifier",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "ArgumentModifiers",
      eLiterals: [
        {
          name: "RESPECT_NULLS",
        },
        {
          name: "IGNORE_NULLS",
          value: 1,
        },
        {
          name: "ENTITY_ESCAPING",
          value: 2,
        },
        {
          name: "NO_ENTITY_ESCAPING",
          value: 3,
        },
        {
          name: "NAME",
          value: 4,
        },
        {
          name: "EVAL_NAME",
          value: 5,
        },
        {
          name: "DOCUMENT",
          value: 6,
        },
        {
          name: "CONTENT",
          value: 7,
        },
        {
          name: "DISTINCT",
          value: 8,
        },
        {
          name: "UNIQUE",
          value: 9,
        },
        {
          name: "CANONICAL",
          value: 10,
        },
        {
          name: "COMPATIBILITY",
          value: 11,
        },
        {
          name: "USING_CHAR_CS",
          value: 12,
        },
        {
          name: "USING_NCHAR_CS",
          value: 13,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "Argument",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Expression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "argument",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "identifier",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "modifier",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ArgumentModifiers",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "FunctionCallExpression",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Expression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "following_expressions",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TypeSpec",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "TimeType",
      eLiterals: [
        {
          name: "LOCAL",
        },
        {
          name: "TIMEZONE",
          value: 1,
        },
        {
          name: "SECOND",
          value: 2,
        },
        {
          name: "HOUR",
          value: 3,
        },
        {
          name: "MINUTE",
          value: 4,
        },
        {
          name: "DAY",
          value: 5,
        },
        {
          name: "MONTH",
          value: 6,
        },
        {
          name: "YEAR",
          value: 7,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "IntervalTypeSpec",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//TypeSpec",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "from_time",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "from_type",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//TimeType",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "to_time",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "to_type",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//TimeType",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DataTypeSpec",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//TypeSpec",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "char_set_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "fractional_precision",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "leading_precision",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "native_type",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//string",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "precision_type",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//string",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "time_type",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//TimeType",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "TypeSpecType",
      eLiterals: [
        {
          name: "ROWTYPE",
        },
        {
          name: "TYPE",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "NameTypeSpec",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//TypeSpec",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "isRef",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "typeSpecType",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//TypeSpecType",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "XmlTableColumn",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "default",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "for_ordinality",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "path",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "XmlElementArgument",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "alias",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "XmlNamespacesClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "default",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "xml_elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//XmlElementArgument",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "XmlPassingClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "by_value",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//XmlElementArgument",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "XmlTableFunction",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//FunctionCallExpression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "columns",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//XmlTableColumn",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "element",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "xml_namespaces_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//XmlNamespacesClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "xml_passing_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//XmlPassingClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "XmlSerializeValue",
      eLiterals: [
        {
          name: "HIDE",
        },
        {
          name: "SHOW",
          value: 1,
        },
        {
          name: "NO_VALUE",
          value: 2,
        },
        {
          name: "INDENT",
          value: 3,
        },
        {
          name: "NO_INDENT",
          value: 4,
        },
        {
          name: "INDENT_WITH_SIZE",
          value: 5,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "XmlSerializeFunction",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//FunctionCallExpression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "argument",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "defaults",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//XmlSerializeValue",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "element",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "encoding",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "indent",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//XmlSerializeValue",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "indent_size",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "modifier",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ArgumentModifiers",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "version",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "XmlRootValue",
      eLiterals: [
        {
          name: "YES",
        },
        {
          name: "NO",
          value: 1,
        },
        {
          name: "NO_VALUE",
          value: 2,
        },
        {
          name: "EXPRESSION",
          value: 3,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "XmlRootFunction",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//FunctionCallExpression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "element",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "standalone",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//XmlRootValue",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "version",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//XmlRootValue",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "version_value",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "XmlQueryFunction",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//FunctionCallExpression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "element",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "null_on_empty",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "xml_passing_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//XmlPassingClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "XmlPiFunction",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//FunctionCallExpression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "element",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression_eval_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "identifier_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "second",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "XmlParseFunction",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//FunctionCallExpression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "argument",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "element",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "modifier",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ArgumentModifiers",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "wellformed",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "XmlExistsFunction",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//FunctionCallExpression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "xml_passing_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//XmlPassingClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "EntityEscaping",
      eLiterals: [
        {
          name: "ENTITY_ESCAPING",
        },
        {
          name: "NO_ENTITY_ESCAPING",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "SchemaCheck",
      eLiterals: [
        {
          name: "SCHEMA_CHECK",
        },
        {
          name: "NO_SCHEMA_CHECK",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "XmlMultiuseElement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "as_eval",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "as_id",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "XmlAttributesClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "entity_escaping",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//EntityEscaping",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "schema_check",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//SchemaCheck",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "xml_multiuse_expression_elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//XmlMultiuseElement",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "XmlElementFunction",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//FunctionCallExpression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "arguments",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//XmlElementArgument",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "element",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "first",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "modifiers",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ArgumentModifiers",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "xml_attributes_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//XmlAttributesClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "XmlMultiuseFunction",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//FunctionCallExpression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "element",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//XmlMultiuseElement",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "AscOrDesc",
      eLiterals: [
        {
          name: "ASC",
        },
        {
          name: "DESC",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "PositionTarget",
      eLiterals: [
        {
          name: "FIRST",
        },
        {
          name: "LAST",
          value: 1,
        },
        {
          name: "ALL",
          value: 2,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "OrderByElements",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "asc_or_desc",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//AscOrDesc",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "nulls",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//PositionTarget",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "OrderByClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "order_by_elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//OrderByElements",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "siblings",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "XmlAggFunction",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//FunctionCallExpression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "element",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "order_by_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//OrderByClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TreatFunction",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//FunctionCallExpression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "ref",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "CursorStatus",
      eLiterals: [
        {
          name: "OPEN",
        },
        {
          name: "FOUND",
          value: 1,
        },
        {
          name: "NOT_FOUND",
          value: 2,
        },
        {
          name: "ROWCOUNT",
          value: 3,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CursorFunction",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//FunctionCallExpression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "cursor_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "status",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//CursorStatus",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "ModelType",
      eLiterals: [
        {
          name: "MODEL",
        },
        {
          name: "MODEL_AUTO",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ValuesClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "values",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CostMatrixClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "model_type",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ModelType",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "names",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "valuesClause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ValuesClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "UsingClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "PredictionFunction",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//FunctionCallExpression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "arguments",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "cost_matrix_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//CostMatrixClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "using_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//UsingClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ExtractFunction",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//FunctionCallExpression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "argument",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//string",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "from",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CollectFunction",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//FunctionCallExpression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "modifier",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ArgumentModifiers",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "order_by_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//OrderByClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CastFunction",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//FunctionCallExpression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "concatenation",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "multiset_subquery",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Subquery",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "DenseRank",
      eLiterals: [
        {
          name: "FIRST",
        },
        {
          name: "LAST",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "QueryPartitionClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expressions",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "subquery",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Subquery",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "WindowingElementsValue",
      eLiterals: [
        {
          name: "UNBOUNDED_PRECEDING",
        },
        {
          name: "CURRENT_ROW",
          value: 1,
        },
        {
          name: "PRECEDING_EXPRESSION",
          value: 2,
        },
        {
          name: "FOLLOWING_EXPRESSION",
          value: 3,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "WindowingElements",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "value",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//WindowingElementsValue",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "WindowingType",
      eLiterals: [
        {
          name: "ROWS",
        },
        {
          name: "RANGE",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "WindowingClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "between_from",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//WindowingElements",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "between_to",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//WindowingElements",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "windowing_elements",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//WindowingElements",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "windowing_type",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//WindowingType",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "OverClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "order_by_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//OrderByClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "query_partition_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//QueryPartitionClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "windowing_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//WindowingClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "KeepClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "dense_rank",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//DenseRank",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "order_by_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//OrderByClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "over_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//OverClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "WithinClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "order_by_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//OrderByClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "WithinOverFunction",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//FunctionCallExpression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "arguments",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "keep_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//KeepClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "over_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//OverClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "within_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//WithinClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ModelingFunction",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//FunctionCallExpression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "arguments",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "external_using_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//UsingClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "internal_using_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//UsingClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "keep_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//KeepClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "NumericArgumentModifier",
      eLiterals: [
        {
          name: "DISTINCT",
        },
        {
          name: "UNIQUE",
          value: 1,
        },
        {
          name: "ALL",
          value: 2,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "MultiColumnForLoop",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expressions",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "paren_column_list",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "subquery",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Subquery",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "SingleColumnForLoopAction",
      eLiterals: [
        {
          name: "INCREMENT",
        },
        {
          name: "DECREMENT",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SingleColumnForLoop",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "action_expr",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "action_type",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//SingleColumnForLoopAction",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expressions",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "from_expr",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "to_expr",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "NumericFunction",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//FunctionCallExpression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "arguments",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "modifier",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//NumericArgumentModifier",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "multi_column_for_loop",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//MultiColumnForLoop",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "over_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//OverClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "single_column_for_loop",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SingleColumnForLoop",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "TrimModifier",
      eLiterals: [
        {
          name: "LEADING",
        },
        {
          name: "TRAILING",
          value: 1,
        },
        {
          name: "BOTH",
          value: 2,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TrimFunctionExpression",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//FunctionCallExpression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "arguments",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "from",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "modifier",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//TrimModifier",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "what",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "GenericFunctionExpression",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//FunctionCallExpression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "arguments",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ModelExpressionElement",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Expression",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ModelExpressionElementSingleForLoop",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ModelExpressionElement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "single_column_for_loops",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SingleColumnForLoop",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ModelExpressionElementMultiForLoop",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ModelExpressionElement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "multi_column_for_loop",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//MultiColumnForLoop",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ModelExpressionElementExpression",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ModelExpressionElement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expressions",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "SubqueryBehavior",
      eLiterals: [
        {
          name: "NONE",
        },
        {
          name: "UNION",
          value: 1,
        },
        {
          name: "UNION_ALL",
          value: 2,
        },
        {
          name: "INTERSECT",
          value: 3,
        },
        {
          name: "MINUS",
          value: 4,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "CrossOrNatural",
      eLiterals: [
        {
          name: "CROSS",
        },
        {
          name: "NATURAL",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "InnerOrOuter",
      eLiterals: [
        {
          name: "INNER",
        },
        {
          name: "OUTER",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "JoinOrApply",
      eLiterals: [
        {
          name: "JOIN",
        },
        {
          name: "APPLY",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "Iterable",
      abstract: true,
      interface: true,
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "Collection",
      abstract: true,
      interface: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Iterable",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "size",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//int",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "List",
      abstract: true,
      interface: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Collection",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "OuterJoinTypeValue",
      eLiterals: [
        {
          name: "FULL",
        },
        {
          name: "LEFT",
          value: 1,
        },
        {
          name: "RIGHT",
          value: 2,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "OuterJoinType",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "outer",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "value",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//OuterJoinTypeValue",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "FlashbackQueryClauseOption",
      eLiterals: [
        {
          name: "VERSIONS_BETWEEN_SCN",
        },
        {
          name: "VERSIONS_BETWEEN_TIMESTAMP",
          value: 1,
        },
        {
          name: "AS_OF_SCN",
          value: 2,
        },
        {
          name: "AS_OF_TIMESTAMP",
          value: 3,
        },
        {
          name: "AS_OF_SNAPSHOT",
          value: 4,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "FlashbackQueryClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//FlashbackQueryClauseOption",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TableRefAuxInternal",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "PivotOrUnpivotClause",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "IncludingOrExcluding",
      eLiterals: [
        {
          name: "INCLUDING",
        },
        {
          name: "EXCLUDING",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "PivotForClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_names",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "UnpivotInElements",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_names",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "constants",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "UnpivotInClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "unpivot_in_elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//UnpivotInElements",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "UnpivotClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//PivotOrUnpivotClause",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_names",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "nulls",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//IncludingOrExcluding",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "pivot_for_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PivotForClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "unpivot_in_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//UnpivotInClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "PivotElement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "aggregate_function_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_alias",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "PivotInClauseElement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_alias",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "PivotInClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "any",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//int",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "pivot_in_clause_elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PivotInClauseElement",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "subquery",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Subquery",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "PivotClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//PivotOrUnpivotClause",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "alias",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "pivot_element",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PivotElement",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "pivot_for_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PivotForClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "pivot_in_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PivotInClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "xml",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TableRefAuxInternalSubquery",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//TableRefAuxInternal",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "pivot_or_unpivot_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PivotOrUnpivotClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "subquery_elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SubqueryElements",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "table_ref",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableRef",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DmlTableExpressionClause",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SampleClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "block",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expressions",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "seed",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "GrantObjectName",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "GrantObjectNameSqlTranslation",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//GrantObjectName",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "schema_object_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "SourceOrResource",
      eLiterals: [
        {
          name: "SOURCE",
        },
        {
          name: "RESOURCE",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "GrantObjectNameJava",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//GrantObjectName",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "schema_object_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "source_or_resource",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//SourceOrResource",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "GrantObjectNameMiningModel",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//GrantObjectName",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "schema_object_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "GrantObjectNameEdition",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//GrantObjectName",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "schema_object_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "GrantObjectNameDirectory",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//GrantObjectName",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "dir_object_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "GrantObjectNameUser",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//GrantObjectName",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "user_object_name",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TableviewName",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//GrantObjectName",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TableviewNameXmlTable",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//TableviewName",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "outer_join_sign",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "xmltable",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//XmlTableFunction",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "PartitionOrSubpartition",
      eLiterals: [
        {
          name: "PARTITION",
        },
        {
          name: "SUBPARTITION",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "PartitionExtensionClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "keys",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "partition",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//PartitionOrSubpartition",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TableviewNameSimple",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//TableviewName",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partition_extension_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PartitionExtensionClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DmlTableExpressionClauseTableview",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//DmlTableExpressionClause",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "sample_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SampleClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tableview_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableviewName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ExecutableElement",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "Directive",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ExecutableElement",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ErrorDirective",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Directive",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "literal",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SelectionDirectiveElsePart",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "seq_of_statements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExecutableElement",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SelectionDirectiveElseifPart",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "condition",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SelectionDirective",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Directive",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "condition",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "selection_directive_else_part",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SelectionDirectiveElsePart",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "selection_directive_elseif_part",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SelectionDirectiveElseifPart",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "seq_of_statements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExecutableElement",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "LabelDeclaration",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ExecutableElement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "label_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SetCommand",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ExecutableElement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//string",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "value",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//string",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "Statement",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ExecutableElement",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DeclareParameter",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "assigned_value",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "collation",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "datatype",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "variable_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DeclareStatement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Statement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parameters",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DeclareParameter",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ExecuteStatement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Statement",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SetStatement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Statement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "value",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "variable",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TryCatchBlock",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Statement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "catch_statements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExecutableElement",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "try_statements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExecutableElement",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ContinueStatement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Statement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "condition",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "label_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "PipeRowStatement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Statement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "RaiseStatement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Statement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "exception_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "GotoStatement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Statement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "label_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "BoundsClause",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "BoundsClauseValues",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//BoundsClause",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "index_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "BetweenBound",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "lower_bound",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "upper_bound",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "BoundsClauseIndices",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//BoundsClause",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "between_bound",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//BetweenBound",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "collection_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "BoundsClauseBetween",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//BoundsClause",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "lower_bound",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "upper_bound",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ForallStatement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Statement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "bounds_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//BoundsClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "index_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "save_exceptions",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "sql_statement",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Statement",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "CompressionStatus",
      eLiterals: [
        {
          name: "COMPRESS",
        },
        {
          name: "NO_COMPRESS",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "KeyCompression",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "compression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "status",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//CompressionStatus",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "LoggingClauseValue",
      eLiterals: [
        {
          name: "LOGGING",
        },
        {
          name: "NOLOGGING",
          value: 1,
        },
        {
          name: "FILESYSTEM_LIKE_LOGGING",
          value: 2,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterTablePropertiesClause",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SupplementalTableLogging",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AlterTablePropertiesClause",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SupplementalDbLoggingTarget",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SupplementalPlsqlClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//SupplementalDbLoggingTarget",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SupplementalDbLoggingTargetData",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//SupplementalDbLoggingTarget",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "SupplementalIdKeyClauseOption",
      eLiterals: [
        {
          name: "ALL",
        },
        {
          name: "PRIMARY_KEY",
          value: 1,
        },
        {
          name: "UNIQUE",
          value: 2,
        },
        {
          name: "FOREIGN_KEY",
          value: 3,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SupplementalIdKeyClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//SupplementalDbLoggingTarget",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "options",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//SupplementalIdKeyClauseOption",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SupplementalTableLoggingDropElement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "log_grp",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "supplemental_id_key_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SupplementalIdKeyClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SupplementalTableLoggingDrop",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//SupplementalTableLogging",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SupplementalTableLoggingDropElement",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SupplementalLogGrpClauseElement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "no_log",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "regular_id",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SupplementalLogGrpClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "always",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SupplementalLogGrpClauseElement",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "log_grp",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SupplementalTableLoggingAddElement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "supplemental_id_key_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SupplementalIdKeyClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "supplemental_log_grp_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SupplementalLogGrpClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SupplementalTableLoggingAdd",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//SupplementalTableLogging",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SupplementalTableLoggingAddElement",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "TableCompression",
      eLiterals: [
        {
          name: "COMPRESS",
        },
        {
          name: "BASIC_COMPRESS",
          value: 1,
        },
        {
          name: "FOR_OLTP_COMPRESS",
          value: 2,
        },
        {
          name: "FOR_QUERY_COMPRESS",
          value: 3,
        },
        {
          name: "FOR_ARCHIVE_COMPRESS",
          value: 4,
        },
        {
          name: "FOR_QUERY_LOW_COMPRESS",
          value: 5,
        },
        {
          name: "FOR_ARCHIVE_LOW_COMPRESS",
          value: 6,
        },
        {
          name: "FOR_QUERY_HIGH_COMPRESS",
          value: 7,
        },
        {
          name: "FOR_ARCHIVE_HIGH_COMPRESS",
          value: 8,
        },
        {
          name: "NO_COMPRESS",
          value: 9,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TableCompressionClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AlterTablePropertiesClause",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//TableCompression",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "CacheOption",
      eLiterals: [
        {
          name: "CACHE",
        },
        {
          name: "NO_CACHE",
          value: 1,
        },
        {
          name: "RESULT_CACHE_DEFAULT",
          value: 2,
        },
        {
          name: "RESULT_CACHE_FORCE",
          value: 3,
        },
        {
          name: "CACHE_READS",
          value: 4,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CacheOptionClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AlterTablePropertiesClause",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//CacheOption",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "FlashbackArchiveClauseOption",
      eLiterals: [
        {
          name: "FLASHBACK_ARCHIVE",
        },
        {
          name: "NO_FLASHBACK_ARCHIVE",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "FlashbackArchiveClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AlterTablePropertiesClause",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "flashback_archive",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//string",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//FlashbackArchiveClauseOption",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "RecordsPerBlockClauseValue",
      eLiterals: [
        {
          name: "MINIMIZE",
        },
        {
          name: "NO_MINIMIZE",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "RecordsPerBlockClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AlterTablePropertiesClause",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "value",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//RecordsPerBlockClauseValue",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ColumnProperties",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "AllowOrDisallow",
      eLiterals: [
        {
          name: "ALLOW",
        },
        {
          name: "DISALLOW",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "XmlschemaSpec",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "any_schema",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//AllowOrDisallow",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "element_id",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//string",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "non_schema",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//AllowOrDisallow",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "xmlschema_id",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//string",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "FileType",
      eLiterals: [
        {
          name: "SECUREFILE",
        },
        {
          name: "BASICFILE",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "Chunk",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "number",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "EncryptOrDecrypt",
      eLiterals: [
        {
          name: "ENCRYPT",
        },
        {
          name: "DECRYPT",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "EncryptionSpec",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "encrypt_algorithm",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "integrity_algorithm",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "password",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "salt",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "Encryption",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "action",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//EncryptOrDecrypt",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "encryption_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//EncryptionSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "Freepools",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "number",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "LobCompressionClause",
      eLiterals: [
        {
          name: "NOCOMPRESS",
        },
        {
          name: "COMPRESS",
          value: 1,
        },
        {
          name: "COMPRESS_HIGH",
          value: 2,
        },
        {
          name: "COMPRESS_MEDIUM",
          value: 3,
        },
        {
          name: "COMPRESS_LOW",
          value: 4,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "LobDeduplicateClause",
      eLiterals: [
        {
          name: "DEDUPLICATE",
        },
        {
          name: "KEEP_DUPLICATES",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "LobParameterCache",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "cache_option",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//CacheOption",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "logging_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//LoggingClauseValue",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "LobRetentionClauseOption",
      eLiterals: [
        {
          name: "RETENTION",
        },
        {
          name: "RETENTION_MAX",
          value: 1,
        },
        {
          name: "RETENTION_MIN",
          value: 2,
        },
        {
          name: "RETENTION_AUTO",
          value: 3,
        },
        {
          name: "RETENTION_NONE",
          value: 4,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "LobRetentionClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//LobRetentionClauseOption",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "value",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "Pctversion",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "number",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "StorageInRow",
      eLiterals: [
        {
          name: "ENABLE_STORAGE_IN_ROW",
        },
        {
          name: "DISABLE_STORAGE_IN_ROW",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "LobParameter",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "chunk",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Chunk",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "encryption",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Encryption",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "freepools",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Freepools",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "lob_compression_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//LobCompressionClause",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "lob_deduplicate_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//LobDeduplicateClause",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "lob_parameter_cache",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//LobParameterCache",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "lob_retention_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//LobRetentionClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "pctversion",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Pctversion",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "storage_in_row",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//StorageInRow",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "LobParameters",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parameters",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//LobParameter",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "XmltypeStore",
      eLiterals: [
        {
          name: "AS_OBJECT_RELATIONAL_CLOB",
        },
        {
          name: "AS_OBJECT_RELATIONAL_BINARY_XML",
          value: 1,
        },
        {
          name: "VARRAYS_AS_LOBS",
          value: 2,
        },
        {
          name: "VARRAYS_AS_TABLES",
          value: 3,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "XmltypeStorage",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "file_type",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//FileType",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "lob_parameters",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//LobParameters",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "lob_segname",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "store_as",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//XmltypeStore",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "XmltypeColumnProperties",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ColumnProperties",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "column",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "xmlschema_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//XmlschemaSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "xmltype_storage",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//XmltypeStorage",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SubstitutableColumnClause",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SubstitutableColumnClauseType",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//SubstitutableColumnClause",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "element",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "is_type",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SubstitutableColumnClauseAllLevels",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//SubstitutableColumnClause",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "substitutable",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "StorageValue",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "StorageOptions",
      eLiterals: [
        {
          name: "INITIAL",
        },
        {
          name: "NEXT",
          value: 1,
        },
        {
          name: "MINEXTENTS",
          value: 2,
        },
        {
          name: "MAXEXTENTS",
          value: 3,
        },
        {
          name: "MINEXTENTS_UNLIMITED",
          value: 4,
        },
        {
          name: "MAXEXTENTS_UNLIMITED",
          value: 5,
        },
        {
          name: "PCTINCREASE",
          value: 6,
        },
        {
          name: "FREELISTS",
          value: 7,
        },
        {
          name: "FREELIST_GROUPS",
          value: 8,
        },
        {
          name: "OPTIMAL",
          value: 9,
        },
        {
          name: "OPTIMAL_NULL",
          value: 10,
        },
        {
          name: "BUFFER_POOL_KEEP",
          value: 11,
        },
        {
          name: "BUFFER_POOL_RECYCLE",
          value: 12,
        },
        {
          name: "BUFFER_POOL_DEFAULT",
          value: 13,
        },
        {
          name: "FLASH_CACHE_KEEP",
          value: 14,
        },
        {
          name: "FLASH_CACHE_NONE",
          value: 15,
        },
        {
          name: "FLASH_CACHE_DEFAULT",
          value: 16,
        },
        {
          name: "ENCRYPT",
          value: 17,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SizeClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "id",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "size",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SizeClauseStorageValue",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//StorageValue",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//StorageOptions",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "size_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SizeClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ExpressionStorageValue",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//StorageValue",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//StorageOptions",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "BasicStorageValue",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//StorageValue",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//StorageOptions",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "StorageClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "options",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StorageValue",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "LobStorageParameters",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "lob_parameters",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//LobParameters",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "storage_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StorageClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tablespace",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "VarrayStorageClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "as_file",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//FileType",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "lob_segname",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "lob_storage_parameters",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//LobStorageParameters",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "VarrayColProperties",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ColumnProperties",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "substitutable_column_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SubstitutableColumnClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "varray_item",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//VarrayItem",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "varray_storage_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//VarrayStorageClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ObjectTypeColProperties",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ColumnProperties",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "substitutable_column_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SubstitutableColumnClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "NestedItemType",
      eLiterals: [
        {
          name: "ID",
        },
        {
          name: "COLUMN_VALUE",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ObjectProperties",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "RelationalProperty",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ObjectProperties",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "ConstraintStateOptionValue",
      eLiterals: [
        {
          name: "DEFERRABLE",
        },
        {
          name: "NOT_DEFERRABLE",
          value: 1,
        },
        {
          name: "INITIALLY_IMMEDIATE",
          value: 2,
        },
        {
          name: "INITIALLY_DEFERRED",
          value: 3,
        },
        {
          name: "RELY",
          value: 4,
        },
        {
          name: "NO_RELY",
          value: 5,
        },
        {
          name: "ENABLE",
          value: 6,
        },
        {
          name: "DISABLE",
          value: 7,
        },
        {
          name: "VALIDATE",
          value: 8,
        },
        {
          name: "NO_VALIDATE",
          value: 9,
        },
        {
          name: "INDEX_CLAUSE",
          value: 10,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ConstraintStateOption",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ConstraintStateOptionValue",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TableIndicator",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "alias",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tableview_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableviewName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ColumnIndicator",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "asc_or_desc",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//AscOrDesc",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "table",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableIndicator",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "IndexProperty",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "GlobalPartitionedIndex",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//IndexProperty",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_names",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "HashPartitionsByQuantity",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "hash_partition_quantity",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "key_compression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//KeyCompression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "overflow_store_in",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "store_in",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "table_compression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//TableCompression",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "LobPartitioningStorage",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "file_type",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//FileType",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "lob_item",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "lob_segname",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tablespace",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "PartitioningStorageClauseOption",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "file_type",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//FileType",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "key_compression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//KeyCompression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "lob_partitioning_storage",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//LobPartitioningStorage",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "lob_segname",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "overflow_tablespace",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "table_compression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//TableCompression",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tablespace",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "varray_item",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//VarrayItem",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "PartitioningStorageClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "options",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PartitioningStorageClauseOption",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "IndividualHashPartitionsClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partition_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partitioning_storage_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PartitioningStorageClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "IndividualHashPartitions",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "individual_hash_partitions_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IndividualHashPartitionsClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "GlobalPartitionedIndexHash",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//GlobalPartitionedIndex",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "hash_partitions_by_quantity",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//HashPartitionsByQuantity",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "individual_hash_partitions",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IndividualHashPartitions",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "IndexPartitioningClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "less_than",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partition_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "segment_attributes_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SegmentAttributesClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "GlobalPartitionedIndexRange",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//GlobalPartitionedIndex",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "index_partitioning_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IndexPartitioningClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "IndexSubpartitionClause",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "IndexSubpartitionSubclause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "key_compression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//KeyCompression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "subpartition_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tablespace",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "unusable",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "IndexSubpartitionClauseSubclause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//IndexSubpartitionClause",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "index_subpartition_subclause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IndexSubpartitionSubclause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "IndexSubpartitionClauseStore",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//IndexSubpartitionClause",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tablespace",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "OnCompPartitionedClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "index_subpartition_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IndexSubpartitionClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "key_compression",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//KeyCompression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partition_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "segment_attributes_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SegmentAttributesClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "OnCompPartitionedTable",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "on_comp_partitioned_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//OnCompPartitionedClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tablespace",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "OnHashPartitionedTable",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "OnHashPartitionedClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "key_compression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//KeyCompression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partition_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tablespace",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "unusable",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "OnHashPartitionedTableClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//OnHashPartitionedTable",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "on_hash_partitioned_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//OnHashPartitionedClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "OnHashPartitionedTableStore",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//OnHashPartitionedTable",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tablespace",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "PartitionedTable",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "key_compression",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//KeyCompression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partition_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "segment_attributes_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SegmentAttributesClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "unusable",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "OnListPartitionedTable",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partitioned_table",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PartitionedTable",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "OnRangePartitionedTable",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partitioned_table",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PartitionedTable",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "LocalPartitionedIndex",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//IndexProperty",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "on_comp_partitioned_table",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//OnCompPartitionedTable",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "on_hash_partitioned_table",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//OnHashPartitionedTable",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "on_list_partitioned_table",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//OnListPartitionedTable",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "on_range_partitioned_table",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//OnRangePartitionedTable",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "XmlIndexParametersClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parameters",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "LocalXmlindexClauseElement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partition_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "xmlindex_parameters_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//XmlIndexParametersClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "LocalXmlindexClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//LocalXmlindexClauseElement",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "XmlindexClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//IndexProperty",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "local_xmlindex_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//LocalXmlindexClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parallel_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ParallelClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "xdb",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "xmlindex_parameters_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//XmlIndexParametersClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "LocalDomainIndexClauseElement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "odci_parameters",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partition_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "LocalDomainIndexClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "local_domain_index_clause_elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//LocalDomainIndexClauseElement",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DomainIndexClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//IndexProperty",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "indextype",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "local_domain_index_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//LocalDomainIndexClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "odci_parameters",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parallel_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ParallelClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ReverseAttribute",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "SortOrNoSort",
      eLiterals: [
        {
          name: "SORT",
        },
        {
          name: "NO_SORT",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TablespaceAttribute",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "is_default",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tablespace",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "VisibleOrInvisible",
      eLiterals: [
        {
          name: "VISIBLE",
        },
        {
          name: "INVISIBLE",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "IndexAttributes",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//IndexProperty",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "key_compression",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//KeyCompression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "logging_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//LoggingClauseValue",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parallel_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ParallelClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "physical_attributes_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PhysicalAttributesClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "reverse",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ReverseAttribute",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "sort_or_nosort",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//SortOrNoSort",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tablespace",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TablespaceAttribute",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "visible_or_invisible",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//VisibleOrInvisible",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "WhereClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "cursor_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "overlaps",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "BitmapJoinIndexClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "columns",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ColumnIndicator",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "from_tables",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableIndicator",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "index_attributes",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IndexAttributes",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "local_partitioned_index",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//LocalPartitionedIndex",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tableview_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableviewName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "where_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//WhereClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ClusterIndexClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "cluster_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "index_attributes",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IndexAttributes",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TableIndexClauseElement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "asc_or_desc",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//AscOrDesc",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "index_expr",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "IndexProperties",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "index_properties",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IndexProperty",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TableIndexClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableIndexClauseElement",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "index_properties",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IndexProperties",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "table_alias",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tableview_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableviewName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "UniqueOrBitmap",
      eLiterals: [
        {
          name: "UNIQUE",
        },
        {
          name: "BITMAP",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreateIndex",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "bitmap_join_index_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//BitmapJoinIndexClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "cluster_index_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ClusterIndexClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "index_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "table_index_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableIndexClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "unique_or_bitmap",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//UniqueOrBitmap",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "unusable",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "UsingIndexClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "create_index",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//CreateIndex",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "index_attributes",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IndexAttributes",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "index_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ConstraintStateOptionIndexClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ConstraintStateOption",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "using_index_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//UsingIndexClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ConstraintStateOptionBasic",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ConstraintStateOption",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ConstraintState",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "options",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ConstraintStateOption",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "InlineConstraint",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "constraint_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "constraint_state",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ConstraintState",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "OnDelete",
      eLiterals: [
        {
          name: "CASCADE",
        },
        {
          name: "SET_NULL",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ReferencesClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "on_delete",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//OnDelete",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "paren_column_list",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tableview_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableviewName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "InlineConstraintReferences",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//InlineConstraint",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "references_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ReferencesClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CheckConstraint",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "condition",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "disable",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "InlineConstraintCheck",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//InlineConstraint",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "check_constraint",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//CheckConstraint",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "ConstraintOption",
      eLiterals: [
        {
          name: "NULL",
        },
        {
          name: "NOT_NULL",
          value: 1,
        },
        {
          name: "UNIQUE",
          value: 2,
        },
        {
          name: "PRIMARY_KEY",
          value: 3,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "InlineConstraintConstant",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//InlineConstraint",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ConstraintOption",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "InlineRefConstraint",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "InlineRefConstraintReferences",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//InlineRefConstraint",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "constraint_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "constraint_state",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ConstraintState",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "references_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ReferencesClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "InlineRefConstraintRowId",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//InlineRefConstraint",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "InlineRefConstraintScope",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//InlineRefConstraint",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tableview_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableviewName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "InlineConstraints",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//RelationalProperty",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "attribute_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "default_expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "inline_constraints",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//InlineConstraint",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "inline_ref_constraint",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//InlineRefConstraint",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SupplementalLoggingProps",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//RelationalProperty",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "supplemental_id_key_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SupplementalIdKeyClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "supplemental_log_grp_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SupplementalLogGrpClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "VirtualColumnDefinition",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//RelationalProperty",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "datatype",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "generated_always",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "inline_constraint",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//InlineConstraint",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "virtual",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ColumnDefinition",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//RelationalProperty",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "collation",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "datatype",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "default_expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "encryption",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//EncryptionSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "inline_constraint",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//InlineConstraint",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "inline_ref_constraint",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//InlineRefConstraint",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "sort",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "OutOfLineRefConstraint",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//RelationalProperty",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ForeignKeyOutOfLineRefConstraint",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//OutOfLineRefConstraint",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "constraint_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "constraint_state",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ConstraintState",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "ref_col_or_attr",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "references_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ReferencesClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "RefOutOfLineRefConstraint",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//OutOfLineRefConstraint",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "ref_col_or_attr",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ScopeOutOfLineRefConstraint",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//OutOfLineRefConstraint",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "ref_col_or_attr",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tableview_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableviewName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "OutOfLineConstraint",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//RelationalProperty",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "constraint_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "constraint_state",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ConstraintState",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CheckOutOfLineConstraint",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//OutOfLineConstraint",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ForeignKeyOutOfLineConstraint",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//OutOfLineConstraint",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_names",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "on_delete_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//OnDelete",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "references_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ReferencesClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "PrimaryOutOfLineConstraint",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//OutOfLineConstraint",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_names",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "UniqueOutOfLineConstraint",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//OutOfLineConstraint",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_names",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "TimingCreation",
      eLiterals: [
        {
          name: "IMMEDIATE",
        },
        {
          name: "DEFERRED",
          value: 1,
        },
        {
          name: "BATCH",
          value: 2,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "PhysicalProperties",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "deferred_segment_creation",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//TimingCreation",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "segment_attributes_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SegmentAttributesClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "table_compression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//TableCompression",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "LocatorOrValue",
      eLiterals: [
        {
          name: "LOCATOR",
        },
        {
          name: "VALUE",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "LocalOrGlobal",
      eLiterals: [
        {
          name: "LOCAL",
        },
        {
          name: "GLOBAL",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "NestedTableColProperties",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ColumnProperties",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_properties",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ColumnProperties",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "nested_item",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "nested_item_type",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//NestedItemType",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "object_properties",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ObjectProperties",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "physical_properties",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PhysicalProperties",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "return_as",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "return_what",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//LocatorOrValue",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "scope",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//LocalOrGlobal",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "substitutable_column_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SubstitutableColumnClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tableview_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableviewName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "LobStorageClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ColumnProperties",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "file_types",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//FileType",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "lob_item",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "lob_segname",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "lob_storage_parameters",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//LobStorageParameters",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "DataInclusion",
      eLiterals: [
        {
          name: "INCLUDING_DATA",
        },
        {
          name: "NOT_INCLUDING_DATA",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "UpgradeTableClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AlterTablePropertiesClause",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_properties",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ColumnProperties",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "data_inclusion",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//DataInclusion",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AllocateExtentClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AlterTablePropertiesClause",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "options",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AllocateExtentClauseOption",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AllocateExtentClauseOption",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AlterTablePropertiesClause",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "datafile",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "inst_num",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "size_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SizeClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DeallocateUnusedClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AlterTablePropertiesClause",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "size_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SizeClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "Parallel",
      eLiterals: [
        {
          name: "PARALLEL",
        },
        {
          name: "NO_PARALLEL",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ParallelClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AlterTablePropertiesClause",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "parallel",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//Parallel",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parallel_count",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "RowMovementClauseValue",
      eLiterals: [
        {
          name: "ENABLE_ROW_MOVEMENT",
        },
        {
          name: "DISABLE_ROW_MOVEMENT",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "RowMovementClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AlterTablePropertiesClause",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "value",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//RowMovementClauseValue",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "LoggingClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AlterTablePropertiesClause",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "value",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//LoggingClauseValue",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "PhysicalAttributesClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AlterTablePropertiesClause",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "inittrans",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "pctfree",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "pctused",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "storage_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StorageClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SegmentAttributesClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "logging_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//LoggingClauseValue",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "physical_attributes_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PhysicalAttributesClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tablespace_name",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "IndexPartitionDescriptionOption",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "key_compression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//KeyCompression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "odci_parameters",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "segment_attributes_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SegmentAttributesClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "IndexPartitionDescription",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "options",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IndexPartitionDescriptionOption",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partition_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "unusable",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SplitIndexPartition",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Statement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "index_partition_description",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IndexPartitionDescription",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "literal",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parallel_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ParallelClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partition_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DropIndexPartition",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Statement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partition_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AddHashIndexPartition",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Statement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "key_compression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//KeyCompression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parallel_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ParallelClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partition_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tablespace",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "RenameIndexPartition",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Statement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partition_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "subpartition_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "to_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ModifyIndexSubpartition",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Statement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "allocate_extent_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AllocateExtentClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "deallocate_unused_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DeallocateUnusedClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "subpartition_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "unusable",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "ModifyIndexPartitionOptionValue",
      eLiterals: [
        {
          name: "COALESCE",
        },
        {
          name: "UPDATE_BLOCK_REFERENCES",
          value: 1,
        },
        {
          name: "UNUSABLE",
          value: 2,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ModifyIndexPartitionOption",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "allocate_extent_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AllocateExtentClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "deallocate_unused_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DeallocateUnusedClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "key_compression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//KeyCompression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "logging_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//LoggingClauseValue",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "odci_parameters",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ModifyIndexPartitionOptionValue",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "physical_attributes_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PhysicalAttributesClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ModifyIndexPartition",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Statement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "options",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ModifyIndexPartitionOption",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partition_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CaseStatement",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Statement",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CaseElsePart",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "seq_of_statements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExecutableElement",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CaseWhenPart",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "condition",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "seq_of_statements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExecutableElement",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SearchedCaseStatement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//CaseStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "case_else_part",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//CaseElsePart",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "case_when_part",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//CaseWhenPart",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "label_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SimpleCaseStatement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//CaseStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "case_else_part",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//CaseElsePart",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "case_when_part",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//CaseWhenPart",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "label_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ExitStatement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Statement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "condition",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "label_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CursorLoopParam",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CursorLoopParamRecord",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//CursorLoopParam",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "cursor_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expressions",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "record_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "select_statement",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SelectStatement",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CursorLoopParamIndex",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//CursorLoopParam",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "index_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "lower_bound",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "reverse",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "upper_bound",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "LoopStatement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Statement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "for_cursor_loop_param",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//CursorLoopParam",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "label_declaration",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//LabelDeclaration",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "label_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "seq_of_statements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExecutableElement",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "while_condition",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "Arguments",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "argument",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Argument",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "keep_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//KeepClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "IndividualCall",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "arguments",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Arguments",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "routine_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "RoutineCall",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Statement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "calls",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IndividualCall",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "keep_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//KeepClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ReturnStatement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Statement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CoalesceIndexPartition",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Statement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parallel_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ParallelClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "IntoClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "bulk_collect",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "into_elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "ReturnOrReturning",
      eLiterals: [
        {
          name: "RETURN",
        },
        {
          name: "RETURNING",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DynamicReturningClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "into_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntoClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "return_or_returning",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ReturnOrReturning",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ExecuteImmediate",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Statement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "dynamic_returning_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DynamicReturningClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "into_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntoClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "using_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//UsingClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CloseStatement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Statement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "cursor_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "FetchStatement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Statement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "bulk_collect",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "cursor_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "variable_names",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "OpenForStatement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Statement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "select_statement",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SelectStatement",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "using_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//UsingClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "variable_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "OpenStatement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Statement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "cursor_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expressions",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DeclareSpec",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "PackageObj",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//DeclareSpec",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DefaultValuePart",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "assign",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "default",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ParameterSpec",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "default_value_part",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DefaultValuePart",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "in_value",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parameter_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CursorDeclaration",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//PackageObj",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "identifier",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "is_statement",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SelectStatement",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parameters",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ParameterSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "return_type",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "VariableDeclaration",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//PackageObj",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "constant",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "default_value_part",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DefaultValuePart",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "identifier",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "not_null",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TypeDeclaration",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//PackageObj",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "identifier",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TypeDeclarationRefCursor",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//TypeDeclaration",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "return_type_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "FieldSpec",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "default_value_part",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DefaultValuePart",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "not_null",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TypeDeclarationRecord",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//TypeDeclaration",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "record_fields",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//FieldSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "VararrayOrVarying",
      eLiterals: [
        {
          name: "VARRAY",
        },
        {
          name: "VARYING_ARRAY",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TypeDeclarationVararray",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//TypeDeclaration",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "not_null",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "vararray_or_varying",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//VararrayOrVarying",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "IndexedOrIndex",
      eLiterals: [
        {
          name: "INDEX",
        },
        {
          name: "INDEXED",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TypeDeclarationTable",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//TypeDeclaration",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "by",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "indexed_or_index",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//IndexedOrIndex",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "not_null",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CallSpec",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "JavaSpec",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//CallSpec",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "java_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CAgentInClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expressions",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CParametersClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expressions",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "variadic",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CSpec",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//CallSpec",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "c_agent_in_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//CAgentInClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "c_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "c_parameters_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//CParametersClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "identifier",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "with_context",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "InvokerRightsClause",
      eLiterals: [
        {
          name: "AUTHID_CURRENT_USER",
        },
        {
          name: "AUTHID_DEFINER",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "ParameterMode",
      eLiterals: [
        {
          name: "IN",
        },
        {
          name: "OUT",
          value: 1,
        },
        {
          name: "INOUT",
          value: 2,
        },
        {
          name: "NOCOPY",
          value: 3,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "Parameter",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "default_value_part",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DefaultValuePart",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "modes",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ParameterMode",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parameter_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ProcedureBody",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//PackageObj",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "body",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Body",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "call_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//CallSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "declare",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "external",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "invoker_rights_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//InvokerRightsClause",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parameters",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Parameter",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "procedure_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "seq_of_declare_specs",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DeclareSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "ByTarget",
      eLiterals: [
        {
          name: "ANY",
        },
        {
          name: "HASH",
          value: 1,
        },
        {
          name: "RANGE",
          value: 2,
        },
        {
          name: "LIST",
          value: 3,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "StreamingSetting",
      eLiterals: [
        {
          name: "ORDER",
        },
        {
          name: "CLUSTER",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "StreamingClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "paren_column_list",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "setting",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//StreamingSetting",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "PartitionByClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "by_target",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ByTarget",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "paren_column_list",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "streaming_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StreamingClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ParallelEnableClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partition_by_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PartitionByClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ResultCacheClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "relies_on_part",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableviewName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "FunctionBody",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//PackageObj",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "deterministic",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "function_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "invoker_rights_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//InvokerRightsClause",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parallel_enable_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ParallelEnableClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parameters",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Parameter",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "result_cache_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ResultCacheClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "PipelinedOrAggregate",
      eLiterals: [
        {
          name: "PIPELINED",
        },
        {
          name: "AGGREGATE",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "FunctionBodyUsing",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//FunctionBody",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "implementation_type_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "pipelined_or_aggregate",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//PipelinedOrAggregate",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "IsOrAs",
      eLiterals: [
        {
          name: "IS",
        },
        {
          name: "AS",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "FunctionBodyDirect",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//FunctionBody",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "body",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Body",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "call_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//CallSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "is_or_as",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//IsOrAs",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "pipelined",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "seq_of_declare_specs",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DeclareSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "PragmaDeclarationOption",
      eLiterals: [
        {
          name: "SERIALLY_REUSABLE",
        },
        {
          name: "AUTONOMOUS_TRANSACTION",
          value: 1,
        },
        {
          name: "EXCEPTION_INIT",
          value: 2,
        },
        {
          name: "INLINE",
          value: 3,
        },
        {
          name: "RESTRICT_REFERENCES",
          value: 4,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "PragmaDeclaration",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//PackageObj",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//PragmaDeclarationOption",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "PragmaDeclarationRestrictReferences",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//PragmaDeclaration",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "first_is_default",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "ids",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "PragmaDeclarationInline",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//PragmaDeclaration",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "inline_id",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "PragmaDeclarationException",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//PragmaDeclaration",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "exception_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "numeric_negative",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//NumericNegative",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "PragmaDeclarationSimple",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//PragmaDeclaration",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SubtypeDeclaration",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//PackageObj",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "identifier",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "not_null",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "range_end",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "range_start",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ExceptionDeclaration",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//PackageObj",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "identifier",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "FunctionSpec",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//PackageObj",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "deterministic",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "identifier",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parameters",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Parameter",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "pipelined",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "result_cache",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "return_type",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ProcedureSpec",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//PackageObj",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "identifier",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parameters",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Parameter",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "Block",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Statement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "body",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Body",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "declare",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "declare_spec",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DeclareSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ExceptionHandler",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "exception_name",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "seq_of_statements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExecutableElement",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "Body",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Statement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "exception_handler",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExceptionHandler",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "label_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "seq_of_statements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExecutableElement",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AssignmentStatement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Statement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "value",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "variable",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "UnitStatement",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Statement",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DropDatabase",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "database_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreateSchema",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "schema_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "ExternalOptionValue",
      eLiterals: [
        {
          name: "CLUSTERED_COLUMNSTORE_INDEX",
        },
        {
          name: "CLUSTERED_COLUMNSTORE_INDEX_ORDER",
          value: 1,
        },
        {
          name: "HEAP",
          value: 2,
        },
        {
          name: "CLUSTERED_INDEX",
          value: 3,
        },
        {
          name: "DISTRIBUTION_HASH",
          value: 4,
        },
        {
          name: "DISTRIBUTION_ROUND_ROBIN",
          value: 5,
        },
        {
          name: "DISTRIBUTION_REPLICATE",
          value: 6,
        },
        {
          name: "TYPE",
          value: 7,
        },
        {
          name: "LOCATION",
          value: 8,
        },
        {
          name: "FORMAT_TYPE",
          value: 9,
        },
        {
          name: "FORMAT_OPTIONS",
          value: 10,
        },
        {
          name: "DATA_SOURCE",
          value: 11,
        },
        {
          name: "FILE_FORMAT",
          value: 12,
        },
        {
          name: "REJECT_TYPE",
          value: 13,
        },
        {
          name: "REJECT_VALUE",
          value: 14,
        },
        {
          name: "LABEL",
          value: 15,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ExternalOption",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ExternalOptionValue",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreateFileFormat",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "format_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "options",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExternalOption",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreateDataSource",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "datasource_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "options",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExternalOption",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreateStatisticsStatement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_names",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "statistics_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tableview_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableviewName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterClusterOption",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "allocate_extent_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AllocateExtentClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "cache_or_nocache",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//CacheOption",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "deallocate_unused_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DeallocateUnusedClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "physical_attributes_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PhysicalAttributesClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "size_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SizeClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterCluster",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "cluster_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "options",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AlterClusterOption",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parallel_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ParallelClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CompilerParametersClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parameter_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parameter_value",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "EnableOrDisable",
      eLiterals: [
        {
          name: "ENABLE",
        },
        {
          name: "DISABLE",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterTrigger",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "alter_trigger_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "compiler_parameters_clauses",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//CompilerParametersClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "debug",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "enable_or_disable",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//EnableOrDisable",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "rename_trigger_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "reuse",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "PublicOrPrivate",
      eLiterals: [
        {
          name: "PUBLIC",
        },
        {
          name: "PRIVATE",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreateOutline",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "can_replace",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "category",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "outline_modifier",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//PublicOrPrivate",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "outline_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "source_modifier",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//PublicOrPrivate",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "source_outline",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "unit_statement",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//UnitStatement",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "NullStatement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AuditUser",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "regular_id",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "ByOrExcept",
      eLiterals: [
        {
          name: "BY",
        },
        {
          name: "EXCEPT",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "Whenever",
      eLiterals: [
        {
          name: "SUCCESSFUL",
        },
        {
          name: "NOT_SUCCESSFUL",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "UnifiedAuditing",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "attribute_names",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "audit_users",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AuditUser",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "by_or_except",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ByOrExcept",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "context_namespace",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "policy_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "whenever",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//Whenever",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "IsolationLevel",
      eLiterals: [
        {
          name: "SERIALIZABLE",
        },
        {
          name: "READ_COMMITTED",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "OpenStatus",
      eLiterals: [
        {
          name: "READ_ONLY",
        },
        {
          name: "READ_WRITE",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SetTransactionStatement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "isolation_level",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//IsolationLevel",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "read",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//OpenStatus",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "rollback_segment",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "SetTarget",
      eLiterals: [
        {
          name: "CONSTRAINT",
        },
        {
          name: "CONSTRAINTS",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SetConstraintStatement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "all",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "constraint_names",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "target",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//SetTarget",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "timing",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//TimingCreation",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SavepointStatement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "savepoint_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "LockMode",
      eLiterals: [
        {
          name: "ROW_SHARE",
        },
        {
          name: "ROW_EXCLUSIVE",
          value: 1,
        },
        {
          name: "SHARE_UPDATE",
          value: 2,
        },
        {
          name: "SHARE",
          value: 3,
        },
        {
          name: "SHARE_ROW_EXCLUSIVE",
          value: 4,
        },
        {
          name: "EXCLUSIVE",
          value: 5,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "LockTableElement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partition_extension_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PartitionExtensionClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tableview_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableviewName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "WaitOrNoWait",
      eLiterals: [
        {
          name: "WAIT",
        },
        {
          name: "NO_WAIT",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "WaitPart",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "wait_or_nowait",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//WaitOrNoWait",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "LockTableStatement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "lock_mode",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//LockMode",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "lock_table_elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//LockTableElement",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "wait_part",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//WaitPart",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "ContainerClause",
      eLiterals: [
        {
          name: "CONTAINER_CURRENT",
        },
        {
          name: "CONTAINER_ALL",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ObjectPrivilege",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "text",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//string",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "Role",
      eLiterals: [
        {
          name: "CONNECT",
        },
        {
          name: "ID",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "RoleName",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "id",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "role",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//Role",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SystemPrivilege",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "text",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//string",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "GrantStatementElement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "object_privilege",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ObjectPrivilege",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "paren_column_list",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "role_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//RoleName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "system_privilege",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SystemPrivilege",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "Grantee",
      eLiterals: [
        {
          name: "NAME",
        },
        {
          name: "PUBLIC",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "IdentifiedBy",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "id_expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "GranteeName",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "id_expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "identified_by",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IdentifiedBy",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "GranteeNameOrPublic",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "grantee",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//Grantee",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "grantee_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//GranteeName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "AdminOrDelegate",
      eLiterals: [
        {
          name: "ADMIN",
        },
        {
          name: "DELEGATE",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "GrantStatement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "container_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ContainerClause",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "grant_option",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "grant_statement_elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//GrantStatementElement",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "grantees",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//GranteeNameOrPublic",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "hierarchy_option",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "on_grant_object",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//GrantObjectName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "with_option",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//AdminOrDelegate",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "RejectLimit",
      eLiterals: [
        {
          name: "UNLIMITED",
        },
        {
          name: "EXPRESSION",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ErrorLoggingRejectPart",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "reject_limit",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//RejectLimit",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ErrorLoggingClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "error_logging_reject_part",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ErrorLoggingRejectPart",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "into_tableview",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableviewName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "MergeInsertClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "values",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "when_not_matched",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "where_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//WhereClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "MergeElement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "MergeUpdateClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "merge_element",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//MergeElement",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "merge_update_delete_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//WhereClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "where_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//WhereClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SelectedTableview",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "select_statement",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SelectStatement",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "table_alias",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tableview_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableviewName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "MergeStatement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "error_logging_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ErrorLoggingClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "into_tableview",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableviewName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "merge_insert_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//MergeInsertClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "merge_update_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//MergeUpdateClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "on_condition",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "table_alias",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "using_selected_tableview",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SelectedTableview",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ExplainStatement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "delete_statement",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DeleteStatement",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "insert_statement",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//InsertStatement",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "into_tableview",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableviewName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "merge_statement",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//MergeStatement",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "select_statement",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SelectStatement",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "statement_id",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "update_statement",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//UpdateStatement",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DropView",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "cascade_constraint",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tableview_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableviewName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "ForceOrValidate",
      eLiterals: [
        {
          name: "FORCE",
        },
        {
          name: "VALIDATE",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DropType",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "body",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "force_or_validate",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ForceOrValidate",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DropTrigger",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "trigger_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DropTable",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "cascade_constraints",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "purge",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tableview_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableviewName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DropSequence",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "sequence_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DropPackage",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "body",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "package_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "schema_object_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TypeBodyElements",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TypeElementsParameter",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parameter_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ConstructorDeclaration",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "body",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Body",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "call_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//CallSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "final",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "function_type",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "instantiable",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "is_or_as",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//IsOrAs",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "self_in_out_type",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "seq_of_declare_specs",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DeclareSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_elements_parameter",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeElementsParameter",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "FuncDeclInType",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "body",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Body",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "call_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//CallSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "declare",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "function_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "is_or_as",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//IsOrAs",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "return_type",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "seq_of_declare_specs",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DeclareSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_elements_parameters",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeElementsParameter",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "MemberOrStatic",
      eLiterals: [
        {
          name: "MEMBER",
        },
        {
          name: "STATIC",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ProcDeclInType",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "body",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Body",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "call_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//CallSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "declare",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "is_or_as",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//IsOrAs",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "procedure_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "seq_of_declare_specs",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DeclareSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_elements_parameter",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeElementsParameter",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SubprogDeclInType",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//TypeBodyElements",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "constructor_declaration",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ConstructorDeclaration",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "func_decl_in_type",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//FuncDeclInType",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "member_or_static",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//MemberOrStatic",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "proc_decl_in_type",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ProcDeclInType",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "ReturnResult",
      eLiterals: [
        {
          name: "SELF",
        },
        {
          name: "TYPE",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "OverridingFunctionSpec",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "body",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Body",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "function_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "is_or_as",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//IsOrAs",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "pipelined",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "return_result",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ReturnResult",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "seq_of_declare_specs",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DeclareSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_elements_parameters",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeElementsParameter",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "OverridingSubprogramSpec",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//TypeBodyElements",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "overriding_function_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//OverridingFunctionSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "MapOrOrder",
      eLiterals: [
        {
          name: "MAP",
        },
        {
          name: "ORDER",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "MapOrderFuncDeclaration",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//TypeBodyElements",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "func_decl_in_type",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//FuncDeclInType",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "map_or_order",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//MapOrOrder",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TypeBody",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "is_or_as",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//IsOrAs",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_body_elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeBodyElements",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "ModifierClause",
      eLiterals: [
        {
          name: "INSTANTIABLE",
        },
        {
          name: "FINAL",
          value: 1,
        },
        {
          name: "OVERRIDING",
          value: 2,
        },
        {
          name: "NOT_INSTANTIABLE",
          value: 3,
        },
        {
          name: "NOT_FINAL",
          value: 4,
        },
        {
          name: "NOT_OVERRIDING",
          value: 5,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "NestedTableTypeDef",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "not_null",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "VarrayTypeDef",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "not_null",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "vararray_or_varying",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//VararrayOrVarying",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ObjectAsPart",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "is_or_as",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//IsOrAs",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "nested_table_type_def",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//NestedTableTypeDef",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "target_is_object",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "varray_type_def",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//VarrayTypeDef",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ElementSpecOptions",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TypeFunctionSpec",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "call_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//CallSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "external_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "external_variable",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "function_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "is_or_as",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//IsOrAs",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "return_result",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ReturnResult",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "return_type",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_elements_parameters",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeElementsParameter",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TypeProcedureSpec",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "call_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//CallSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "is_or_as",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//IsOrAs",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "procedure_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_elements_parameter",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeElementsParameter",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SubprogramSpec",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ElementSpecOptions",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "member_or_static",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//MemberOrStatic",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_function_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeFunctionSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_procedure_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeProcedureSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "MapOrderFunctionSpec",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ElementSpecOptions",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "map_or_order",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//MapOrOrder",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_function_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeFunctionSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ConstructorSpec",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ElementSpecOptions",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "call_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//CallSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "final",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "function_type",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "instantiable",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "is_or_as",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//IsOrAs",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "self_in_out_type",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_elements_parameter",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeElementsParameter",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "PragmaElement",
      eLiterals: [
        {
          name: "DEFAULT",
        },
        {
          name: "IDENTIFIER",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "PragmaElements",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "identifier",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "pragma_element",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//PragmaElement",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "PragmaClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "pragma_elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PragmaElements",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ElementSpec",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "element_spec_options",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementSpecOptions",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "modifier_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ModifierClause",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "pragma_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PragmaClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SqljObjectTypeAttr",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "external_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ObjectMemberSpec",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "element_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "identifier",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "sqlj_object_type_attr",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SqljObjectTypeAttr",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ObjectUnderPart",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "JavaSource",
      eLiterals: [
        {
          name: "SQLDATA",
        },
        {
          name: "CUSTOMDATUM",
          value: 1,
        },
        {
          name: "ORADATA",
          value: 2,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SqljObjectType",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "external_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "source",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//JavaSource",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ObjectTypeDef",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "invoker_rights_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//InvokerRightsClause",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "modifier_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ModifierClause",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "object_as_part",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ObjectAsPart",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "object_member_spec",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ObjectMemberSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "object_under_part",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ObjectUnderPart",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "sqlj_object_type",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SqljObjectType",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TypeDefinition",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "object_type_def",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ObjectTypeDef",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "oid",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreateType",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "can_replace",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_body",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeBody",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_definition",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeDefinition",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DmlOrNotTrigger",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "Activation",
      eLiterals: [
        {
          name: "BEFORE",
        },
        {
          name: "AFTER",
          value: 1,
        },
        {
          name: "INSTEAD_OF",
          value: 2,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "NonDmlEvent",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "text",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//string",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "DatabaseOrSchema",
      eLiterals: [
        {
          name: "DATABASE",
        },
        {
          name: "SCHEMA",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "NonDmlTrigger",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//DmlOrNotTrigger",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "activation",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//Activation",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "non_dml_events",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//NonDmlEvent",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "on_what",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//DatabaseOrSchema",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "schema_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "SqlOperation",
      eLiterals: [
        {
          name: "ALTER",
        },
        {
          name: "AUDIT",
          value: 1,
        },
        {
          name: "COMMENT",
          value: 2,
        },
        {
          name: "DELETE",
          value: 3,
        },
        {
          name: "EXECUTE",
          value: 4,
        },
        {
          name: "FLASHBACK",
          value: 5,
        },
        {
          name: "GRANT",
          value: 6,
        },
        {
          name: "INDEX",
          value: 7,
        },
        {
          name: "INSERT",
          value: 8,
        },
        {
          name: "LOCK",
          value: 9,
        },
        {
          name: "READ",
          value: 10,
        },
        {
          name: "RENAME",
          value: 11,
        },
        {
          name: "SELECT",
          value: 12,
        },
        {
          name: "UPDATE",
          value: 13,
        },
        {
          name: "ALL",
          value: 14,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DmlEventElement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_list",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "operation",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//SqlOperation",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DmlEventNestedClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tableview_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableviewName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DmlEventClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "dml_event_elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DmlEventElement",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "dml_event_nested_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DmlEventNestedClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tableview_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableviewName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ForEachRow",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "ReferencingTarget",
      eLiterals: [
        {
          name: "NEW",
        },
        {
          name: "OLD",
          value: 1,
        },
        {
          name: "PARENT",
          value: 2,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ReferencingElement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_alias",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "target",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ReferencingTarget",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ReferencingClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "referencing_element",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ReferencingElement",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SimpleDmlTrigger",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//DmlOrNotTrigger",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "activation",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//Activation",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "dml_event_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DmlEventClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "for_each_row",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ForEachRow",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "referencing_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ReferencingClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CompoundDmlTrigger",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//DmlOrNotTrigger",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "dml_event_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DmlEventClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "referencing_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ReferencingClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "TriggerBodyOption",
      eLiterals: [
        {
          name: "COMPOUND_TRIGGER",
        },
        {
          name: "CALL",
          value: 1,
        },
        {
          name: "TRIGGER_BLOCK",
          value: 2,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TriggerBlock",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "body",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Body",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "declare",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "declare_spec",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DeclareSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TriggerBody",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "identifier",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//TriggerBodyOption",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "trigger_block",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TriggerBlock",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TriggerFollowsClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "trigger_names",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TriggerWhenClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "condition",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreateTrigger",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "can_replace",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "enable_or_disable",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//EnableOrDisable",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "trigger",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DmlOrNotTrigger",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "trigger_body",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TriggerBody",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "trigger_follows_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TriggerFollowsClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "trigger_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "trigger_when_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TriggerWhenClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "FileSize",
      eLiterals: [
        {
          name: "BIGFILE",
        },
        {
          name: "SMALLFILE",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "FileSpecification",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "RedoLogFileSpec",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//FileSpecification",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "blocksize",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SizeClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "datafile_names",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "reuse",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "size",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SizeClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "MaxsizeClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "size_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SizeClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "unlimited",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AutoextendClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "maxsize_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//MaxsizeClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "off",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "size_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SizeClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DatafileTempfileSpec",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//FileSpecification",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "autoextend_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AutoextendClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "filename",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "reuse",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "size_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SizeClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DatafileSpecification",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "datafile_tempfile_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DatafileTempfileSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "ExtentManagementClauseOption",
      eLiterals: [
        {
          name: "AUTOALLOCATE",
        },
        {
          name: "UNIFORM",
          value: 1,
        },
        {
          name: "UNIFORM_SIZE",
          value: 2,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ExtentManagementClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ExtentManagementClauseOption",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "size_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SizeClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterDatabaseOption",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "Replication",
      eLiterals: [
        {
          name: "LOGICAL",
        },
        {
          name: "PHYSICAL",
          value: 1,
        },
        {
          name: "SNAPSHOT",
          value: 2,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ActivateStandbyDbClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "finish_apply",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "logical_or_physical",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//Replication",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "CommitSwitchoverOption",
      eLiterals: [
        {
          name: "PRIMARY",
        },
        {
          name: "STANDBY",
          value: 1,
        },
        {
          name: "SESSION_SHUTDOWN",
          value: 2,
        },
        {
          name: "CANCEL",
          value: 3,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "WithOrWithout",
      eLiterals: [
        {
          name: "WITH",
        },
        {
          name: "WITHOUT",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CommitSwitchoverClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "logical_or_physical",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//Replication",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//CommitSwitchoverOption",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "session_shutdown_wait_or_nowait",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//WaitOrNoWait",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "session_shutdown_with_or_without",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//WithOrWithout",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ConvertDatabaseClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "replication",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//Replication",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "MaximizeOption",
      eLiterals: [
        {
          name: "PROTECTION",
        },
        {
          name: "AVAILABILITY",
          value: 1,
        },
        {
          name: "PERFORMANCE",
          value: 2,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "MaximizeStandbyDbClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//MaximizeOption",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "RegisterLogfileClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "can_replace",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "file_specifications",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//FileSpecification",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "logical_or_physical",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//Replication",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "logminer_session_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "StartStandbyClauseOption",
      eLiterals: [
        {
          name: "NEW_PRIMARY",
        },
        {
          name: "INITIAL",
          value: 1,
        },
        {
          name: "SKIP_FAILED_TRANSACTION",
          value: 2,
        },
        {
          name: "FINISH",
          value: 3,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "StartStandbyClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "immediate",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "new_primary_id",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "no_delay",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//StartStandbyClauseOption",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "scn_value",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "StopOrAbort",
      eLiterals: [
        {
          name: "STOP",
        },
        {
          name: "ABORT",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "StopStandbyClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "stop_or_abort",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//StopOrAbort",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "StandbyDatabaseClauses",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AlterDatabaseOption",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "activate_standby_db_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ActivateStandbyDbClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "commit_switchover_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//CommitSwitchoverClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "convert_database_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ConvertDatabaseClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "maximize_standby_db_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//MaximizeStandbyDbClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parallel_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ParallelClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "register_logfile_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//RegisterLogfileClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "start_standby_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StartStandbyClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "stop_standby_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StopStandbyClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "LogfileClauses",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AlterDatabaseOption",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "ArchiveOption",
      eLiterals: [
        {
          name: "ARCHIVE",
        },
        {
          name: "NO_ARCHIVE",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ArchiveLogfileClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//LogfileClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "manual",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ArchiveOption",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "RenameLogfileClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//LogfileClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "filename",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "to_filename",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "LogfileDescriptor",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "filenames",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "group",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ClearLogfileClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//LogfileClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "logfile_descriptor",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//LogfileDescriptor",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "unarchived",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "unrecoverable",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ForceLoggingLogfileClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//LogfileClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "no_force",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SupplementalDbLogging",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//LogfileClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "action",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//Actions",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "target",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SupplementalDbLoggingTarget",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SwitchLogfileClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//LogfileClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "blocksize",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DropLogfileClauses",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//LogfileClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "filename",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "logfile_descriptor",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//LogfileDescriptor",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "standby",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "LogFilenameReuse",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "filename",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "reuse",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "LogGroupSpec",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "group",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "redo_log_file_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//RedoLogFileSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AddLogfileClauses",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//LogfileClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "descriptors",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//LogfileDescriptor",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "filenames",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//LogFilenameReuse",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "group_specs",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//LogGroupSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "instance",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "isStandby",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "thread_number",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "SecurityClauseValue",
      eLiterals: [
        {
          name: "GUARD_ALL",
        },
        {
          name: "GUARD_STANDBY",
          value: 1,
        },
        {
          name: "GUARD_NONE",
          value: 2,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SecurityClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AlterDatabaseOption",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "value",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//SecurityClauseValue",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "InstanceClauses",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AlterDatabaseOption",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "enable_or_disable",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//EnableOrDisable",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "instance",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ControlfileClauses",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AlterDatabaseOption",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "ResetLogsOrNoResetLogs",
      eLiterals: [
        {
          name: "RESET_LOGS",
        },
        {
          name: "NO_RESET_LOGS",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TraceFileClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "filename",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "resetlogs_or_noresetlogs",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ResetLogsOrNoResetLogs",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "reuse",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ControlfileClausesBackup",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ControlfileClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "filename",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "reuse",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "trace_file_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TraceFileClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ControlfileClausesStandby",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ControlfileClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "filename",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "logical_or_physical",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//Replication",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "reuse",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DatabaseFileClauses",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AlterDatabaseOption",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "RenameFileClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//DatabaseFileClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "to_filename",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "what_filename",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "Filenumber",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "number",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreateDatafileClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//DatabaseFileClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "asNew",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "file_specifications",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//FileSpecification",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "filenames",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "filenumbers",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Filenumber",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "AlterFileType",
      eLiterals: [
        {
          name: "TEMPFILE",
        },
        {
          name: "DATAFILE",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "AlterFileOption",
      eLiterals: [
        {
          name: "ONLINE",
        },
        {
          name: "OFFLINE",
          value: 1,
        },
        {
          name: "OFFLINE_FOR_DROP",
          value: 2,
        },
        {
          name: "RESIZE",
          value: 3,
        },
        {
          name: "AUTOEXTEND",
          value: 4,
        },
        {
          name: "END_BACKUP",
          value: 5,
        },
        {
          name: "DROP",
          value: 6,
        },
        {
          name: "DROP_INCLUDING_DATAFILES",
          value: 7,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterFileClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//DatabaseFileClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "alterType",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//AlterFileType",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "autoextend_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AutoextendClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "filenames",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "filenumbers",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Filenumber",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//AlterFileOption",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "size_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SizeClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "RecoveryClauses",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AlterDatabaseOption",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ManagedStandbyRecovery",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//RecoveryClauses",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "ManagedStandbyRecoveryLogicalOption",
      eLiterals: [
        {
          name: "DB_NAME",
        },
        {
          name: "KEEP_IDENTITY",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ManagedStandbyRecoveryLogical",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ManagedStandbyRecovery",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "db_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ManagedStandbyRecoveryLogicalOption",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "ManagedStandbyRecoveryDatabaseOption",
      eLiterals: [
        {
          name: "FINISH",
        },
        {
          name: "CANCEL",
          value: 1,
        },
        {
          name: "RECOVERY_CLAUSE",
          value: 2,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "ManagedStandbyRecoveryClauseValue",
      eLiterals: [
        {
          name: "USING_CURRENT_LOGFILE",
        },
        {
          name: "DISCONNECT",
          value: 1,
        },
        {
          name: "DISCONNECT_FROM_SESSION",
          value: 2,
        },
        {
          name: "NO_DELAY",
          value: 3,
        },
        {
          name: "UNTIL_CHANGE",
          value: 4,
        },
        {
          name: "UNTIL_CONSISTENT",
          value: 5,
        },
        {
          name: "PARALLEL_CLAUSE",
          value: 6,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ManagedStandbyRecoveryClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "change",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parallel_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ParallelClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "value",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ManagedStandbyRecoveryClauseValue",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ManagedStandbyRecoveryDatabase",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ManagedStandbyRecovery",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ManagedStandbyRecoveryDatabaseOption",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "recovery_clauses",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ManagedStandbyRecoveryClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "GeneralRecoveryOption",
      eLiterals: [
        {
          name: "DATABASE",
        },
        {
          name: "CONTINUE",
          value: 1,
        },
        {
          name: "CONTINUE_DEFAULT",
          value: 2,
        },
        {
          name: "CANCEL",
          value: 3,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "GeneralRecovery",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//RecoveryClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "automatic",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "from",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//GeneralRecoveryOption",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "GeneralRecoveryValue",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//GeneralRecovery",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "FullDatabaseRecoveryElementOption",
      eLiterals: [
        {
          name: "UNTIL_CANCEL",
        },
        {
          name: "UNTIL_TIME",
          value: 1,
        },
        {
          name: "UNTIL_CHANGE",
          value: 2,
        },
        {
          name: "UNTIL_CONSISTENT",
          value: 3,
        },
        {
          name: "USING_BACKUP_CONTROLFILE",
          value: 4,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "FullDatabaseRecoveryElement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "change",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//FullDatabaseRecoveryElementOption",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "time",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "FullDatabaseRecovery",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//FullDatabaseRecoveryElement",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "standby",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "PartialDatabaseRecovery",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "PartialDatabaseRecovery10g",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//PartialDatabaseRecovery",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "element",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PartialDatabaseRecoveryElement",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "standby",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "until_consistent",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "PartialDatabaseRecoveryElement",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//PartialDatabaseRecovery",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "PartialDatabaseRecoveryElementDatafile",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//PartialDatabaseRecoveryElement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "datafile",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "PartialDatabaseRecoveryElementFilenumber",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//PartialDatabaseRecoveryElement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "filenumbers",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Filenumber",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "strings",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "PartialDatabaseRecoveryElementTablespace",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//PartialDatabaseRecoveryElement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tablespace",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "GeneralRecoveryDatabase",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//GeneralRecovery",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "database_option",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//GeneralRecoveryDatabaseOption",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "full_database_recovery",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//FullDatabaseRecovery",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "logfile",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partial_database_recovery",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PartialDatabaseRecovery",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "GeneralRecoveryDatabaseOptionValue",
      eLiterals: [
        {
          name: "TEST",
        },
        {
          name: "ALLOW_CORRUPTION",
          value: 1,
        },
        {
          name: "PARALLEL_CLAUSE",
          value: 2,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "GeneralRecoveryDatabaseOption",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//RecoveryClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "corruption",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parallel_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ParallelClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "value",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//GeneralRecoveryDatabaseOptionValue",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "RecoveryBackupOption",
      eLiterals: [
        {
          name: "BEGIN_BACKUP",
        },
        {
          name: "END_BACKUP",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "RecoveryClausesBackup",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//RecoveryClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//RecoveryBackupOption",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "StartupClauses",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AlterDatabaseOption",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "MountOption",
      eLiterals: [
        {
          name: "STANDBY",
        },
        {
          name: "CLONE",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "StartupClauseMount",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//StartupClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//MountOption",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "UpgradeOrDowngrade",
      eLiterals: [
        {
          name: "UPGRADE",
        },
        {
          name: "DOWNGRADE",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "StartupClauseOpen",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//StartupClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "resetlogs_or_noresetlogs",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ResetLogsOrNoResetLogs",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "status",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//OpenStatus",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "upgrade_or_downgrade",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//UpgradeOrDowngrade",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DefaultSettingsClause",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AlterDatabaseOption",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SetTimeZoneClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//DefaultSettingsClause",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "timezone",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DefaultSettingsClauseChangeTracking",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//DefaultSettingsClause",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "enable_or_disable",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//EnableOrDisable",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "filename",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "reuse",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DefaultSettingsClauseGlobalName",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//DefaultSettingsClause",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "database",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "domain",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DefaultSettingsClauseTablespace",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//DefaultSettingsClause",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tablespace",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tablespace_group_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "temporary",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DefaultSettingsClauseFile",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//DefaultSettingsClause",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "filesize",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//FileSize",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DefaultSettingsClauseEdition",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//DefaultSettingsClause",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "edition_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "OnOrOff",
      eLiterals: [
        {
          name: "ON",
        },
        {
          name: "OFF",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "FlashbackModeClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//DefaultSettingsClause",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//OnOrOff",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "PermanentTablespaceClauseOptionValue",
      eLiterals: [
        {
          name: "MINIMUM_EXTENT",
        },
        {
          name: "BLOCKSIZE",
          value: 1,
        },
        {
          name: "LOGGING_CLAUSE",
          value: 2,
        },
        {
          name: "FORCE_LOGGING",
          value: 3,
        },
        {
          name: "ONLINE",
          value: 4,
        },
        {
          name: "OFFLINE",
          value: 5,
        },
        {
          name: "ENCRYPTION",
          value: 6,
        },
        {
          name: "DEFAULT",
          value: 7,
        },
        {
          name: "EXTENT_MANAGEMENT_CLAUSE",
          value: 8,
        },
        {
          name: "SEGMENT_MANAGEMENT_CLAUSE",
          value: 9,
        },
        {
          name: "FLASHBACK_MODE_CLAUSE",
          value: 10,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "SegmentManagementClause",
      eLiterals: [
        {
          name: "SEGMENT_MANAGEMENT_AUTO",
        },
        {
          name: "SEGMENT_MANAGEMENT_MANUAL",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TablespaceEncryptionSpec",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "encrypt_algorithm",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "PermanentTablespaceClauseOption",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "extent_management_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtentManagementClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "flashback_mode_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//FlashbackModeClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "logging_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//LoggingClauseValue",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//PermanentTablespaceClauseOptionValue",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "segment_management_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//SegmentManagementClause",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "size_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SizeClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "storage_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StorageClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "table_compression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//TableCompression",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tablespace_encryption_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TablespaceEncryptionSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "PermanentTablespaceClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "datafile_specification",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DatafileSpecification",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "id_expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "permanent_tablespace_clause_options",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PermanentTablespaceClauseOption",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TablespaceGroupClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tablespace_group_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tablespace_group_string",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TempfileSpecification",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "datafile_tempfile_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DatafileTempfileSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TemporaryTablespaceClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "extent_management_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtentManagementClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tablespace_group_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TablespaceGroupClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tablespace_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tempfile_specification",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TempfileSpecification",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "TablespaceRetentionClause",
      eLiterals: [
        {
          name: "RETENTION_GUARANTEE",
        },
        {
          name: "RETENTION_NO_GUARANTEE",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "UndoTablespaceClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "datafile_specification",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DatafileSpecification",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "extent_management_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtentManagementClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tablespace_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "tablespace_retention_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//TablespaceRetentionClause",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreateTablespace",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "file_size",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//FileSize",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "permanent_tablespace_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PermanentTablespaceClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "temporary_tablespace_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TemporaryTablespaceClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "undo_tablespace_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//UndoTablespaceClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreateSynonym",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "can_replace",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "for_schema_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "is_public",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "link_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "schema_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "schema_object_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "synonym_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreateDirectory",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "can_replace",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "directory_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "directory_path",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "CreateContextOption",
      eLiterals: [
        {
          name: "INITIALIZED_EXTERNALLY",
        },
        {
          name: "INITIALIZED_GLOBALLY",
          value: 1,
        },
        {
          name: "ACCESSED_GLOBALLY",
          value: 2,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreateContext",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "can_replace",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//CreateContextOption",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "oracle_namespace",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "package_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "using_schema_object_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "CommitOption",
      eLiterals: [
        {
          name: "COMMENT",
        },
        {
          name: "FORCE_CORRUPT_XID",
          value: 1,
        },
        {
          name: "FORCE_CORRUPT_XID_ALL",
          value: 2,
        },
        {
          name: "FORCE_EXPRESSIONS",
          value: 3,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "WriteClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "timing",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//TimingCreation",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "wait_or_nowait",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//WaitOrNoWait",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CommitStatement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expressions",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//CommitOption",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "work",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "write_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//WriteClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "SettingTarget",
      eLiterals: [
        {
          name: "DEFAULT",
        },
        {
          name: "ID",
          value: 1,
        },
        {
          name: "EXPRESSION",
          value: 2,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TablespaceSetting",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tablespace",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "target",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//SettingTarget",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ModifyIndexDefaultAttrs",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "for_partition_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "logging_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//LoggingClauseValue",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "physical_attributes_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PhysicalAttributesClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tablespace",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TablespaceSetting",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DropIndex",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "index_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterIndex",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "index_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "MvLogPurgeClauseValue",
      eLiterals: [
        {
          name: "IMMEDIATE",
        },
        {
          name: "IMMEDIATE_SYNCHRONOUS",
          value: 1,
        },
        {
          name: "IMMEDIATE_ASYNCHRONOUS",
          value: 2,
        },
        {
          name: "START_WITH",
          value: 3,
        },
        {
          name: "NEXT",
          value: 4,
        },
        {
          name: "REPEAT_INTERVAL",
          value: 5,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "MvLogPurgeClauseElement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "value",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//MvLogPurgeClauseValue",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "MvLogPurgeClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//MvLogPurgeClauseElement",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreateMaterializedViewLogOption",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "cache_option",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//CacheOption",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "logging_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//LoggingClauseValue",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "physical_attributes_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PhysicalAttributesClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tablespace_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TablePartitioningClauses",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TablePartitionDescription",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "deferred_segment_creation",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//TimingCreation",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "key_compression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//KeyCompression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "lob_storage_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//LobStorageClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "nested_table_col_properties",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//NestedTableColProperties",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "overflow",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "overflow_segment_attributes",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SegmentAttributesClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "table_compression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//TableCompression",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "varray_col_properties",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//VarrayColProperties",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ReferencePartitionDesc",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partition_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "table_partition_description",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TablePartitionDescription",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SystemPartitioning",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//TablePartitioningClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partitions_value",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "reference_partition_desc",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ReferencePartitionDesc",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ReferencePartitioning",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//TablePartitioningClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "reference_partition_desc",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ReferencePartitionDesc",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "regular_id",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "RangeValuesClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "less_than_values",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "RangePartitionsClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partition_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "range_values_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//RangeValuesClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "table_partition_description",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TablePartitionDescription",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "RangePartitions",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//TablePartitioningClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "interval_expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "range",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "range_partitions_clauses",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//RangePartitionsClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "store_in",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ListValuesClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "values",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ListPartitionsClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "list_values_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ListValuesClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partition_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "table_partition_description",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TablePartitionDescription",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ListPartitions",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//TablePartitioningClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "list_partitions_clauses",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ListPartitionsClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "HashPartitions",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//TablePartitioningClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_name",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "hash_partitions_by_quantity",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//HashPartitionsByQuantity",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "individual_hash_partitions",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IndividualHashPartitions",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "HashSubpartsByQuantity",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "store_in",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "value",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "IndividualHashSubparts",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partitioning_storage_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PartitioningStorageClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "subpartition_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ListSubpartitionDesc",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "list_values_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ListValuesClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partitioning_storage_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PartitioningStorageClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "subpartition_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "RangeSubpartitionDesc",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partitioning_storage_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PartitioningStorageClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "range_values_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//RangeValuesClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "subpartition_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "PartitionDescClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "hash_subparts_by_quantity",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//HashSubpartsByQuantity",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "individual_hash_subparts",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IndividualHashSubparts",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "list_subpartition_desc",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ListSubpartitionDesc",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "range_subpartition_desc",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//RangeSubpartitionDesc",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "RangePartitionDesc",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partition_desc_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PartitionDescClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partition_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "range_values_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//RangeValuesClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "table_partition_description",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TablePartitionDescription",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SubpartitionTemplate",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "hash_subpartition_quantity",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "individual_hash_subparts",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IndividualHashSubparts",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "list_subpartition_desc",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ListSubpartitionDesc",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "range_subpartition_desc",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//RangeSubpartitionDesc",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SubpartitionByHash",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_name",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "store_in",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "subpartition_template",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SubpartitionTemplate",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "value",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SubpartitionByList",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "subpartition_template",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SubpartitionTemplate",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SubpartitionByRange",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_name",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "subpartition_template",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SubpartitionTemplate",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CompositeRangePartitions",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//TablePartitioningClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "interval_expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "range",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "range_partition_desc",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//RangePartitionDesc",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "store_in",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "subpartition_by_hash",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SubpartitionByHash",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "subpartition_by_list",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SubpartitionByList",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "subpartition_by_range",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SubpartitionByRange",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ListPartitionDesc",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "list_values_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ListValuesClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partition_desc_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PartitionDescClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partition_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "table_partition_description",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TablePartitionDescription",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CompositeListPartitions",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//TablePartitioningClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "list_partition_desc",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ListPartitionDesc",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "subpartition_by_hash",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SubpartitionByHash",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "subpartition_by_list",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SubpartitionByList",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "subpartition_by_range",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SubpartitionByRange",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CompositeHashPartitions",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//TablePartitioningClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_name",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "hash_partitions_by_quantity",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//HashPartitionsByQuantity",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "individual_hash_partitions",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IndividualHashPartitions",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "subpartition_by_hash",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SubpartitionByHash",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "subpartition_by_list",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SubpartitionByList",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "subpartition_by_range",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SubpartitionByRange",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "NewValuesClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "value",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//IncludingOrExcluding",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "WithClausesElement",
      eLiterals: [
        {
          name: "OBJECT_ID",
        },
        {
          name: "PRIMARY_KEY",
          value: 1,
        },
        {
          name: "ROWID",
          value: 2,
        },
        {
          name: "SEQUENCE",
          value: 3,
        },
        {
          name: "COMMIT_SCN",
          value: 4,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "WithClauses",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "new_values_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//NewValuesClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "regular_id",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "with_clauses_elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//WithClausesElement",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreateMaterializedViewLog",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "mv_log_purge_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//MvLogPurgeClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "options",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//CreateMaterializedViewLogOption",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parallel_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ParallelClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "table_partitioning_clauses",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TablePartitioningClauses",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tableview_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableviewName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "with_clauses",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//WithClauses",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "BuildClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "timing",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//TimingCreation",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ConstraintsOrAliases",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ConstraintsOrAliasesAlias",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ConstraintsOrAliases",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "alias",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "encryption",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//EncryptionSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ConstraintsOrAliasesScopedConstraint",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ConstraintsOrAliases",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "scoped_constraint",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ScopeOutOfLineRefConstraint",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreateMaterializedViewUsingIndexClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "mv_tablespace",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "physical_attributes_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PhysicalAttributesClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "CreateMvRefreshValue",
      eLiterals: [
        {
          name: "NEVER_REFRESH",
        },
        {
          name: "FAST",
          value: 1,
        },
        {
          name: "COMPLETE",
          value: 2,
        },
        {
          name: "FORCE",
          value: 3,
        },
        {
          name: "ON_DEMAND",
          value: 4,
        },
        {
          name: "ON_COMMIT",
          value: 5,
        },
        {
          name: "START_WITH",
          value: 6,
        },
        {
          name: "NEXT",
          value: 7,
        },
        {
          name: "WITH_PRIMARY_KEY",
          value: 8,
        },
        {
          name: "WITH_ROWID",
          value: 9,
        },
        {
          name: "USING_ENFORCED_CONSTRAINTS",
          value: 10,
        },
        {
          name: "USING_TRUSTED_CONSTRAINTS",
          value: 11,
        },
        {
          name: "DEFAULT_ROLLBACK_SEGMENT",
          value: 12,
        },
        {
          name: "DEFAULT_MASTER_ROLLBACK_SEGMENT",
          value: 13,
        },
        {
          name: "DEFAULT_LOCAL_ROLLBACK_SEGMENT",
          value: 14,
        },
        {
          name: "ROLLBACK_SEGMENT",
          value: 15,
        },
        {
          name: "MASTER_ROLLBACK_SEGMENT",
          value: 16,
        },
        {
          name: "LOCAL_ROLLBACK_SEGMENT",
          value: 17,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "RefreshCondition",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "condition",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//CreateMvRefreshValue",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "date",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "rb_segment",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//string",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreateMvRefresh",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "conditions",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//RefreshCondition",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "IndexOrNoIndex",
      eLiterals: [
        {
          name: "INDEX",
        },
        {
          name: "NO_INDEX",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "PrebuiltTable",
      eLiterals: [
        {
          name: "PREBUILT_TABLE",
        },
        {
          name: "PREBUILT_TABLE_WITH_REDUCED_PRECISION",
          value: 1,
        },
        {
          name: "PREBUILT_TABLE_WITHOUT_REDUCED_PRECISION",
          value: 2,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "QueryRewrite",
      eLiterals: [
        {
          name: "ENABLE_QUERY_REWRITE",
        },
        {
          name: "DISABLE_QUERY_REWRITE",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CycleClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_list",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "default_expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "to_expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "DepthOrBreadth",
      eLiterals: [
        {
          name: "DEPTH",
        },
        {
          name: "BREADTH",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SearchClauseElement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "asc_or_desc",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//AscOrDesc",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "nulls",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//PositionTarget",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SearchClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "depth_or_breadth",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//DepthOrBreadth",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SearchClauseElement",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "FactoringElement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "cycle_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//CycleClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "order_by_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//OrderByClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "paren_column_list",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "query_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "search_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SearchClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "subquery",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Subquery",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "FactoringClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "factoring_elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//FactoringElement",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "Query",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "factoring_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//FactoringClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "subquery",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Subquery",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreateMaterializedView",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "build_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//BuildClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "cache_or_nocache",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//CacheOption",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "constraints_or_aliases",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ConstraintsOrAliases",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "create_materialized_view_using_index_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//CreateMaterializedViewUsingIndexClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "create_mv_refresh",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//CreateMvRefresh",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "for_update",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "index_or_noindex",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//IndexOrNoIndex",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parallel_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ParallelClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "physical_properties",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PhysicalProperties",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "prebuilt_table",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//PrebuiltTable",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "query_rewrite",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//QueryRewrite",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "select_only_statement",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Query",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tableview_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableviewName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "ReadOnlyOrCheckOption",
      eLiterals: [
        {
          name: "READ_ONLY",
        },
        {
          name: "CHECK_OPTION",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SubqueryRestrictionClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "constraint_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "readonly_or_checkoption",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ReadOnlyOrCheckOption",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ViewOptions",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ViewAliasConstraint",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ViewOptions",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ViewAliasConstraintElement",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "out_of_line_constraint",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//OutOfLineConstraint",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "table_alias",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ViewAliasConstraintElement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ViewOptions",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "inline_constraint",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//InlineConstraint",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "out_of_line_constraint",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//OutOfLineConstraint",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "table_alias",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "WithObject",
      eLiterals: [
        {
          name: "IDENTIFIER",
        },
        {
          name: "ID",
          value: 1,
        },
        {
          name: "OID",
          value: 2,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "XmltypeViewClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ViewOptions",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expressions",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "with_object_target",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//WithObject",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "with_object_value",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//SettingTarget",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "xmlschema_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//XmlschemaSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ObjectViewClauseConstraint",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "id",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "inline_constraint",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//InlineConstraint",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "out_of_line_constraint",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//OutOfLineConstraint",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ObjectViewClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ViewOptions",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "ids",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "object_view_clause_constraint",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ObjectViewClauseConstraint",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tableview_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableviewName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "with_object_target",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//WithObject",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "with_object_value",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//SettingTarget",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreateView",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "can_force",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "can_replace",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "editionable",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "editioning",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "select_only_statement",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Query",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "subquery_restriction_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SubqueryRestrictionClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tableview_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableviewName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "view_options",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ViewOptions",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterDatabase",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "controlfile_clauses",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ControlfileClauses",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "database_file_clauses",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DatabaseFileClauses",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "database_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "default_settings_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DefaultSettingsClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "instance_clauses",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//InstanceClauses",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "logfile_clauses",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//LogfileClauses",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "recovery_clauses",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//RecoveryClauses",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "security_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SecurityClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "standby_database_clauses",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StandbyDatabaseClauses",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "startup_clauses",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StartupClauses",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreateClusterElement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "datatype",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "sort",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "CreateClusterOptionValue",
      eLiterals: [
        {
          name: "PHYSICAL_ATTRIBUTES",
        },
        {
          name: "SIZE",
          value: 1,
        },
        {
          name: "TABLESPACE",
          value: 2,
        },
        {
          name: "INDEX",
          value: 3,
        },
        {
          name: "HASHKEYS",
          value: 4,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreateClusterOption",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "value",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//CreateClusterOptionValue",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreateClusterOptionHashkeys",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//CreateClusterOption",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "hash_is",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "key",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "single_table",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreateClusterOptionIndex",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//CreateClusterOption",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreateClusterOptionTablespace",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//CreateClusterOption",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tablespace",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreateClusterOptionSize",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//CreateClusterOption",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "size_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SizeClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreateClusterOptionPhysicalAttributes",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//CreateClusterOption",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "physical_attributes_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PhysicalAttributesClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "RowDependencies",
      eLiterals: [
        {
          name: "ROWDEPENDENCIES",
        },
        {
          name: "NO_ROWDEPENDENCIES",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreateCluster",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "cache_or_nocache",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//CacheOption",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "cluster_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//CreateClusterElement",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "options",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//CreateClusterOption",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parallel_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ParallelClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "row_dependencies",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//RowDependencies",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "CreateTableOption",
      eLiterals: [
        {
          name: "GLOBAL_TEMPORARY",
        },
        {
          name: "EXTERNAL",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreateTable",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "as_statement",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Query",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "create_table_option",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//CreateTableOption",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tableview_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableviewName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TargetElement",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TargetElementConstraint",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//TargetElement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "constraint_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TargetElementUnique",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//TargetElement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_names",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TargetElementPrimaryKey",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//TargetElement",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ExceptionsClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tableview_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableviewName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "KeepOrDrop",
      eLiterals: [
        {
          name: "KEEP",
        },
        {
          name: "DROP",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "ValidateOrNoValidate",
      eLiterals: [
        {
          name: "VALIDATE",
        },
        {
          name: "NO_VALIDATE",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "EnableDisableClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "cascade",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "element",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TargetElement",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "enable_or_disable",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//EnableOrDisable",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "exceptions_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExceptionsClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "index",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//KeepOrDrop",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "using_index_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//UsingIndexClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "validate_or_novalidate",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ValidateOrNoValidate",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "OidClause",
      eLiterals: [
        {
          name: "SYSTEM_GENERATED",
        },
        {
          name: "PRIMARY_KEY",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "OidIndexClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "index_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "physical_attributes_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PhysicalAttributesClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tablespace",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "OnCommit",
      eLiterals: [
        {
          name: "DELETE_ROWS",
        },
        {
          name: "PRESERVE_ROWS",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "XmltypeVirtualColumnsElement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "as_expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "XmltypeVirtualColumns",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//XmltypeVirtualColumnsElement",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "XmltypeTable",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "cache_or_no_cache",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//CacheOption",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_properties",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ColumnProperties",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "enable_disable_clauses",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//EnableDisableClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "flashback_archive_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//FlashbackArchiveClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "object_properties",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ObjectProperties",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "oid_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//OidClause",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "oid_index_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//OidIndexClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "on_commit",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//OnCommit",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parallel_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ParallelClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "physical_properties",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PhysicalProperties",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "result_cache",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//CacheOption",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "row_dependencies",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//RowDependencies",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "row_movement_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//RowMovementClauseValue",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "table_partitioning_clauses",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TablePartitioningClauses",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "xmlschema_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//XmlschemaSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "xmltype_storage",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//XmltypeStorage",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "xmltype_virtual_columns",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//XmltypeVirtualColumns",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreateTableXml",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//CreateTable",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "xmltype_table",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//XmltypeTable",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "ObjectTableSubstitution",
      eLiterals: [
        {
          name: "NOT_SUBSTITUTABLE",
        },
        {
          name: "SUBSTITUTABLE",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ObjectTable",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "cache_or_no_cache",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//CacheOption",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_properties",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ColumnProperties",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "enable_disable_clauses",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//EnableDisableClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "flashback_archive_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//FlashbackArchiveClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "object_properties",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ObjectProperties",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "object_table_substitution",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ObjectTableSubstitution",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "of_type",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "oid_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//OidClause",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "oid_index_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//OidIndexClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "on_commit",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//OnCommit",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parallel_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ParallelClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "physical_properties",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PhysicalProperties",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "result_cache",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//CacheOption",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "row_dependencies",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//RowDependencies",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "row_movement_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//RowMovementClauseValue",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "table_partitioning_clauses",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TablePartitioningClauses",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreateTableObject",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//CreateTable",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "object_table",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ObjectTable",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreateTableAzure",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//CreateTable",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "relational_properties",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//RelationalProperty",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "table_options",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExternalOption",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreateTableRelational",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//CreateTable",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "cache",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//CacheOption",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_properties",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ColumnProperties",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "enable_disable_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//EnableDisableClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "flashback_archive_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//FlashbackArchiveClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "on_commit",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//OnCommit",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parallel_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ParallelClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "physical_properties",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PhysicalProperties",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "relational_properties",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//RelationalProperty",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "result_cache",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//CacheOption",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "row_dependencies",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//RowDependencies",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "row_movement_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//RowMovementClauseValue",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "table_partitioning_clauses",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TablePartitioningClauses",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "GeneralTableRef",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "dml_table_expression_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DmlTableExpressionClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "only",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "table_alias",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "StaticReturningClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expressions",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "into_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntoClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "return_or_returning",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ReturnOrReturning",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DeleteStatement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "error_logging_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ErrorLoggingClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "from",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "general_table_ref",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//GeneralTableRef",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "static_returning_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StaticReturningClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "where_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//WhereClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ColumnBasedUpdateSetClause",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ColumnBasedUpdateSetClauseSubquery",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ColumnBasedUpdateSetClause",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "paren_column_list",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "subquery",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Subquery",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ColumnBasedUpdateSetClauseExpression",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ColumnBasedUpdateSetClause",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "UpdateSetClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_based_update_set_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ColumnBasedUpdateSetClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "identifier",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "UpdateStatement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "error_logging_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ErrorLoggingClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "from_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//FromClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "general_table_ref",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//GeneralTableRef",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "static_returning_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StaticReturningClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "update_set_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//UpdateSetClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "where_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//WhereClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "RollbackStatement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "force",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "savepoint",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "savepoint_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "work",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "SequenceSpecOption",
      eLiterals: [
        {
          name: "INCREMENT_BY",
        },
        {
          name: "MAXVALUE",
          value: 1,
        },
        {
          name: "NOMAXVALUE",
          value: 2,
        },
        {
          name: "MINVALUE",
          value: 3,
        },
        {
          name: "NOMINVALUE",
          value: 4,
        },
        {
          name: "CYCLE",
          value: 5,
        },
        {
          name: "NOCYCLE",
          value: 6,
        },
        {
          name: "CACHE",
          value: 7,
        },
        {
          name: "NOCACHE",
          value: 8,
        },
        {
          name: "ORDER",
          value: 9,
        },
        {
          name: "NOORDER",
          value: 10,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SequenceSpec",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//SequenceSpecOption",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "value",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SequenceStartClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "value",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreateSequence",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "sequence_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "sequence_spec",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SequenceSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "sequence_start_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SequenceStartClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DropProcedure",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "procedure_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DropFunction",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "function_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CallStatement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "calls",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IndividualCall",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "keep_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//KeepClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ElseIfPart",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "condition",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "seq_of_statements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExecutableElement",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ElsePart",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "seq_of_statements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExecutableElement",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "IfStatement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "condition",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "else_if_part",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElseIfPart",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "else_part",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElsePart",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "seq_of_statements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExecutableElement",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreatePackage",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "can_replace",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "invoker_rights_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//InvokerRightsClause",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "is_or_as",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//IsOrAs",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "package_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "package_obj_spec",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PackageObj",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "schema_object_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreatePackageBody",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "can_replace",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "is_or_as",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//IsOrAs",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "package_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "package_obj_body",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PackageObj",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "schema_object_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "seq_of_statements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExecutableElement",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreateProcedureBody",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "body",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Body",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "call_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//CallSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "can_replace",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "declare",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "external",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "invoker_rights_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//InvokerRightsClause",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parameters",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Parameter",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "procedure_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "seq_of_declare_specs",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DeclareSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreateFunctionBody",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "can_replace",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "deterministic",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "function_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "invoker_rights_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//InvokerRightsClause",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parallel_enable_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ParallelEnableClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parameters",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Parameter",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "result_cache_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ResultCacheClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreateFunctionBodyUsing",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//CreateFunctionBody",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "implementation_type_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "pipelined_or_aggregate",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//PipelinedOrAggregate",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreateFunctionBodyDirect",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//CreateFunctionBody",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "body",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Body",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "call_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//CallSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "is_or_as",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//IsOrAs",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "pipelined",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "seq_of_declare_specs",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DeclareSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "AuditContainerClause",
      eLiterals: [
        {
          name: "CURRENT",
        },
        {
          name: "ALL",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "SessionOrAccess",
      eLiterals: [
        {
          name: "SESSION",
        },
        {
          name: "ACCESS",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AuditTraditional",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "audit_container_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//AuditContainerClause",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "by",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//SessionOrAccess",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "whenever",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//Whenever",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AuditingByClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "audit_user",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AuditUser",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AuditDirectPath",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "auditing_by_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AuditingByClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AuditTraditionalDirectPath",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AuditTraditional",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "audit_direct_path",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AuditDirectPath",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AuditTraditionalNetwork",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AuditTraditional",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "AuditingTarget",
      eLiterals: [
        {
          name: "OBJECT",
        },
        {
          name: "DIRECTORY",
          value: 1,
        },
        {
          name: "MINING_MODEL",
          value: 2,
        },
        {
          name: "SQL_TRANSLATION_PROFILE",
          value: 3,
        },
        {
          name: "DEFAULT",
          value: 4,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AuditingOnClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "auditing_target",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//AuditingTarget",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "id",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AuditSchemaObjectClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "auditing_on_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AuditingOnClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "sql_operation",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//SqlOperation",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AuditTraditionalSchema",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AuditTraditional",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "audit_schema_object_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AuditSchemaObjectClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "StatementsOrPrivileges",
      eLiterals: [
        {
          name: "STATEMENTS",
        },
        {
          name: "PRIVILEGES",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AuditOperationClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "operations",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//string",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "statementsOrPrivileges",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//StatementsOrPrivileges",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AuditTraditionalOperation",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AuditTraditional",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "audit_operation_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AuditOperationClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "auditing_by_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AuditingByClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "in_session_current",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "UsingStatisticsType",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "statistics_type",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ColumnAssociation",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_name",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tableview_name",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableviewName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "using_statistics_type",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//UsingStatisticsType",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DefaultCostClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "cpu_cost",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "io_cost",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "network_cost",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DefaultSelectivityClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "default_selectivity",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "FunctionAssociationElement",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "FunctionAssociationElementIndexTypes",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//FunctionAssociationElement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "indextypes_names",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "FunctionAssociationElementIndexes",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//FunctionAssociationElement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "indexes_names",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "FunctionAssociationElementTypes",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//FunctionAssociationElement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "types_names",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "FunctionAssociationElementPackages",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//FunctionAssociationElement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "packages_names",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "FunctionAssociationElementFunctions",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//FunctionAssociationElement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "functions_names",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "FunctionAssociation",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "default_cost_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DefaultCostClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "default_selectivity_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DefaultSelectivityClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "function_association_element",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//FunctionAssociationElement",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "using_statistics_type",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//UsingStatisticsType",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "StorageTableClause",
      eLiterals: [
        {
          name: "WITH_SYSTEM_MANAGED_STORAGE_TABLES",
        },
        {
          name: "WITH_USER_MANAGED_STORAGE_TABLES",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AssociateStatistics",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_association",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ColumnAssociation",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "function_association",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//FunctionAssociation",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "storage_table_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//StorageTableClause",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AnonymousBlock",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "exception_handler",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExceptionHandler",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "seq_of_declare_specs",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DeclareSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "seq_of_statements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExecutableElement",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DeleteStatistics",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "system",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ListRows",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "into",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tableview_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableviewName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ColumnsDesc",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "attribute_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "size",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "ForClauseOption",
      eLiterals: [
        {
          name: "TABLE",
        },
        {
          name: "ALL_COLUMNS",
          value: 1,
        },
        {
          name: "ALL_INDEXED_COLUMNS",
          value: 2,
        },
        {
          name: "COLUMNS",
          value: 3,
        },
        {
          name: "ALL_LOCAL_INDEXES",
          value: 4,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ForClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "columns_desc",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ColumnsDesc",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ForClauseOption",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "size",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "StatisticsClauses",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "for_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ForClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "system",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "RowsOrPercent",
      eLiterals: [
        {
          name: "ROWS",
        },
        {
          name: "PERCENT",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "EstimateStatisticsClauses",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//StatisticsClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "sample_unit",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//RowsOrPercent",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "sample_value",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ComputeStatisticsClauses",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//StatisticsClauses",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ValidationClause",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "OnlineOrOffline",
      eLiterals: [
        {
          name: "ONLINE",
        },
        {
          name: "OFFLINE",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ValidateStructureClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ValidationClause",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "cascade",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "fast",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "into_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntoClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "online_or_offline",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//OnlineOrOffline",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ValidateRefUpdateClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ValidationClause",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "set_dangling_to_null",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AnalyzeObject",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AnalyzeCluster",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AnalyzeObject",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "cluster_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AnalyzeIndex",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AnalyzeObject",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "index_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partition_extention_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PartitionExtensionClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AnalyzeTable",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AnalyzeObject",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partition_extention_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PartitionExtensionClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tableview_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableviewName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "Analyze",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "delete_statistics",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DeleteStatistics",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "list_rows",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ListRows",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "statistics_clauses",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StatisticsClauses",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "validation_clauses",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ValidationClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "what",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AnalyzeObject",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "AlterViewOptions",
      eLiterals: [
        {
          name: "ADD",
        },
        {
          name: "MODIFY_CONSTRAINT",
          value: 1,
        },
        {
          name: "DROP_CONSTRAINT",
          value: 2,
        },
        {
          name: "DROP_PRIMARY_KEY",
          value: 3,
        },
        {
          name: "DROP_UNIQUE",
          value: 4,
        },
        {
          name: "COMPILE",
          value: 5,
        },
        {
          name: "READ_ONLY",
          value: 6,
        },
        {
          name: "READ_WRITE",
          value: 7,
        },
        {
          name: "EDITIONABLE",
          value: 8,
        },
        {
          name: "NON_EDITIONABLE",
          value: 9,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterView",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//AlterViewOptions",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tableview_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableviewName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DropUniqueAlterView",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AlterView",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_names",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DropConstraintAlterView",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AlterView",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "constraint_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ModifyConstraintAlterView",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AlterView",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "constraint_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "rely",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AddConstraintAlterView",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AlterView",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "out_of_line_constraint",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//OutOfLineConstraint",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SimpleAlterView",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AlterView",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "Identified",
      eLiterals: [
        {
          name: "EXTERNALLY",
        },
        {
          name: "GLOBALLY",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "IdentifiedOtherClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "identified",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//Identified",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "identified_as",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "PasswordExpireClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ProfileClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "id",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "QuotaSize",
      eLiterals: [
        {
          name: "SIZE_CLAUSE",
        },
        {
          name: "UNLIMITED",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "QuotaClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "on_id",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "quota_size",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//QuotaSize",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "size_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SizeClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "EnableEditionsClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "UserLockClause",
      eLiterals: [
        {
          name: "ACCOUNT_LOCK",
        },
        {
          name: "ACCOUNT_UNLOCK",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "DefaultOrTemporary",
      eLiterals: [
        {
          name: "DEFAULT",
        },
        {
          name: "TEMPORARY",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "UserTablespaceClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "default_or_temporary",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//DefaultOrTemporary",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "id_expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CreateUser",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "container_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ContainerClause",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "identified_by",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IdentifiedBy",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "identified_other_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IdentifiedOtherClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "password_expire_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PasswordExpireClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "profile_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ProfileClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "quota_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//QuotaClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "user_editions_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//EnableEditionsClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "user_lock_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//UserLockClause",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "user_object_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "user_tablespace_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//UserTablespaceClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterIdentifiedBy",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "identified_by",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IdentifiedBy",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "replace",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterUserEditionsClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "for_which",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "force",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "ContainerScope",
      eLiterals: [
        {
          name: "ALL",
        },
        {
          name: "DEFAULT",
          value: 1,
        },
        {
          name: "NAMED",
          value: 2,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ContainerData",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "action",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//Actions",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "container_names",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "scope",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ContainerScope",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ContainerDataClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "add_rem_container_data",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ContainerData",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "container_tableview_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "set_container_data",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ContainerData",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "Authentication",
      eLiterals: [
        {
          name: "PASSWORD",
        },
        {
          name: "CERTIFICATE",
          value: 1,
        },
        {
          name: "DISTINGUISHED_NAME",
          value: 2,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "ProxyThrough",
      eLiterals: [
        {
          name: "ENTERPRISE_USERS",
        },
        {
          name: "USER",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "RevokeOrGrant",
      eLiterals: [
        {
          name: "REVOKE",
        },
        {
          name: "GRANT",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "RoleClauseTarget",
      eLiterals: [
        {
          name: "ALL",
        },
        {
          name: "ALL_EXCEPT_NAMED",
          value: 1,
        },
        {
          name: "NAMED",
          value: 2,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "RoleClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "role_names",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//RoleName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "target",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//RoleClauseTarget",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "WithRoles",
      eLiterals: [
        {
          name: "NO_ROLES",
        },
        {
          name: "ROLE_CLAUSE",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ProxyClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "authenticated_using",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//Authentication",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "authentication_required",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "proxy_through",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ProxyThrough",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "revoke_or_grant",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//RevokeOrGrant",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "role_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//RoleClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "user_object_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "with_roles",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//WithRoles",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "DefaultRole",
      eLiterals: [
        {
          name: "NONE",
        },
        {
          name: "ROLE_CLAUSE",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "UserDefaultRoleClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "role",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//DefaultRole",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "role_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//RoleClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterUser",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "alter_identified_by",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AlterIdentifiedBy",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "alter_user_editions_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AlterUserEditionsClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "container_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ContainerClause",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "container_data_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ContainerDataClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "identified_other_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IdentifiedOtherClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "password_expire_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PasswordExpireClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "profile_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ProfileClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "proxy_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ProxyClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "quota_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//QuotaClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "user_default_role_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//UserDefaultRoleClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "user_lock_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//UserLockClause",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "user_object_name",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "user_tablespace_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//UserTablespaceClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterCollectionClauses",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "element_type",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "limit",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterMethodElement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "action",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//Actions",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "map_order_function_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//MapOrderFunctionSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "subprogram_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SubprogramSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterMethodSpec",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "alter_method_elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AlterMethodElement",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "ConstructScope",
      eLiterals: [
        {
          name: "PACKAGE",
        },
        {
          name: "BODY",
          value: 1,
        },
        {
          name: "SPECIFICATION",
          value: 2,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CompileTypeClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "compiler_parameters_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//CompilerParametersClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "debug",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "reuse_settings",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "scope",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ConstructScope",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DependentExceptionsPart",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "force",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tableview_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableviewName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "DependentHandlingClauseOption",
      eLiterals: [
        {
          name: "INVALIDATE",
        },
        {
          name: "CASCADE",
          value: 1,
        },
        {
          name: "CONVERT_TO_SUBSTITUTABLE",
          value: 2,
        },
        {
          name: "NOT_INCLUDING_TABLE_DATA",
          value: 3,
        },
        {
          name: "INCLUDING_TABLE_DATA",
          value: 4,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DependentHandlingClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "dependent_exceptions_part",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DependentExceptionsPart",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "options",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//DependentHandlingClauseOption",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ReplaceTypeClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "invoker_rights_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//InvokerRightsClause",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "object_member_specs",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ObjectMemberSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterType",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "alter_attribute",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AlterAttribute",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "alter_collection_clauses",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AlterCollectionClauses",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "alter_method_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AlterMethodSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "compile_type_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//CompileTypeClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "dependent_handling_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DependentHandlingClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "modifier_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ModifierClause",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "overriding_subprogram_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//OverridingSubprogramSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "replace_type_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ReplaceTypeClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "BeginOrEnd",
      eLiterals: [
        {
          name: "BEGIN",
        },
        {
          name: "END",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterTablespaceBackup",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "value",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//BeginOrEnd",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "ChangeType",
      eLiterals: [
        {
          name: "SHRINK",
        },
        {
          name: "RESIZE",
          value: 1,
        },
        {
          name: "MINIMUM_EXTENT",
          value: 2,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterTablespaceChangeSize",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "change_type",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ChangeType",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "keep_size",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SizeClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterTablespaceCoalesce",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DatafileTempfileClauses",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DatafileTempfileClausesOnlineOrOffline",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//DatafileTempfileClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "file_type",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//AlterFileType",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "online_or_offline",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//OnlineOrOffline",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DatafileTempfileClausesRename",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//DatafileTempfileClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "rename_filenames",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "to_filenames",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DatafileTempfileClausesShrink",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//DatafileTempfileClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "filename",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "number_file",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "size_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SizeClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DatafileTempfileClausesDrop",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//DatafileTempfileClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "file_type",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//AlterFileType",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "filename",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "number_file",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "size_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SizeClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DatafileTempfileClausesAdd",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//DatafileTempfileClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "datafile_specification",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DatafileSpecification",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tempfile_specification",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TempfileSpecification",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterTablespaceDefault",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "storage_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StorageClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "table_compression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//TableCompression",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterTablespaceRename",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tablespace",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "TablespaceLoggingClausesValue",
      eLiterals: [
        {
          name: "LOGGING_CLAUSE",
        },
        {
          name: "FORCE_LOGGING",
          value: 1,
        },
        {
          name: "NO_FORCE_LOGGING",
          value: 2,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TablespaceLoggingClauses",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "logging_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//LoggingClauseValue",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "value",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//TablespaceLoggingClausesValue",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "TablespaceStateClauses",
      eLiterals: [
        {
          name: "ONLINE",
        },
        {
          name: "OFFLINE_NORMAL",
          value: 1,
        },
        {
          name: "OFFLINE_TEMPORARY",
          value: 2,
        },
        {
          name: "OFFLINE_IMMEDIATE",
          value: 3,
        },
        {
          name: "OFFLINE",
          value: 4,
        },
        {
          name: "READ_ONLY",
          value: 5,
        },
        {
          name: "READ_WRITE",
          value: 6,
        },
        {
          name: "PERMANENT",
          value: 7,
        },
        {
          name: "TEMPORARY",
          value: 8,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterTablespace",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "autoextend_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AutoextendClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "backup_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AlterTablespaceBackup",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "change_size_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AlterTablespaceChangeSize",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "coalesce_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AlterTablespaceCoalesce",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "datafile_tempfile_clauses",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DatafileTempfileClauses",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "default_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AlterTablespaceDefault",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "flashback_mode_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//FlashbackModeClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "rename_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AlterTablespaceRename",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tablespace",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tablespace_group_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TablespaceGroupClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tablespace_logging_clauses",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TablespaceLoggingClauses",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "tablespace_retention_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//TablespaceRetentionClause",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "tablespace_state_clauses",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//TablespaceStateClauses",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "OutOfLinePartitionStorageElement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "lob_storage_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//LobStorageClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "nested_table_col_properties",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//NestedTableColProperties",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "varray_col_properties",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//VarrayColProperties",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "OutOfLinePartitionStorageSubpartition",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//OutOfLinePartitionStorageElement",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "subpartition_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "OutOfLinePartitionStorage",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//OutOfLinePartitionStorageElement",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partition_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "subpartitions",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//OutOfLinePartitionStorageSubpartition",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AddColumnClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_definitions",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//RelationalProperty",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_properties",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ColumnProperties",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "out_of_line_partition_storage",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//OutOfLinePartitionStorage",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "virtual_column_definition",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//VirtualColumnDefinition",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "CascadeClause",
      eLiterals: [
        {
          name: "CASCADE_CONSTRAINTS",
        },
        {
          name: "INVALIDATE",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "DropColumnClauseType",
      eLiterals: [
        {
          name: "DROP_COLUMNS",
        },
        {
          name: "DROP_UNUSED_COLUMNS",
          value: 1,
        },
        {
          name: "DROP_COLUMNS_CONTINUE",
          value: 2,
        },
        {
          name: "SET_UNUSED",
          value: 3,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DropColumnClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "cascade_clauses",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//CascadeClause",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "checkpoint",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_names",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "drop_type",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//DropColumnClauseType",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ExternalTableDataPropsElement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "directory_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "location",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parameter_string",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parameter_subquery",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Subquery",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ExternalTableDataProps",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExternalTableDataPropsElement",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "AlterXmlschemaClause",
      eLiterals: [
        {
          name: "ALLOW_ANYSCHEMA",
        },
        {
          name: "ALLOW_NONSCHEMA",
          value: 1,
        },
        {
          name: "DISALLOW_NONSCHEMA",
          value: 2,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ModifyColProperties",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "alter_xmlschema_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//AlterXmlschemaClause",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "datatype",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeSpec",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "default_expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "encryption",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Encryption",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "inline_constraint",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//InlineConstraint",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "lob_storage_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//LobStorageClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ModifyColSubstitutable",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "force",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "not_substitutable",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ModifyColumnClauses",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "modify_col_properties",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ModifyColProperties",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "modify_col_substitutable",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ModifyColSubstitutable",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "ProjectColumn",
      eLiterals: [
        {
          name: "ALL",
        },
        {
          name: "REFERENCED",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterExternalTableElement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "add_column_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AddColumnClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "drop_column_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DropColumnClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "error_logging_reject_part",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ErrorLoggingRejectPart",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "external_table_data_props",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExternalTableDataProps",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "modify_column_clauses",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ModifyColumnClauses",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parallel_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ParallelClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "project_column",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ProjectColumn",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterExternalTable",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AlterExternalTableElement",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "PartitioningSetting",
      eLiterals: [
        {
          name: "AUTOMATIC",
        },
        {
          name: "MANUAL",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterAutomaticPartitioning",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "partitioning_setting",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//PartitioningSetting",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "store_in",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ModifyTablePartition",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AddHashSubpartition",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "individual_hash_subparts",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IndividualHashSubparts",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AddListSubpartition",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "list_subpartition_desc",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ListSubpartitionDesc",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AddRangeSubpartition",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "range_subpartition_desc",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//RangeSubpartitionDesc",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "GlobalIndexClause",
      eLiterals: [
        {
          name: "UPDATE",
        },
        {
          name: "INVALIDATE",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CoalesceTableSubpartition",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "clustering",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//AllowOrDisallow",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parallel_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ParallelClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "subpartition_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "update_index_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//GlobalIndexClause",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "LocalIndexes",
      eLiterals: [
        {
          name: "REBUILD_UNUSABLE_LOCAL_INDEXES",
        },
        {
          name: "UNUSABLE_LOCAL_INDEXES",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ShrinkClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "cascade",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "compact",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "PartitionAttributesElement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "allocate_extent_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AllocateExtentClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "deallocate_unused_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DeallocateUnusedClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "logging_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//LoggingClauseValue",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "overflow",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "physical_attributes_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PhysicalAttributesClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "shrink_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ShrinkClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "PartitionAttributes",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PartitionAttributesElement",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "table_compression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//TableCompression",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ModifyTablePartitionList",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ModifyTablePartition",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "add_hash_subpartition",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AddHashSubpartition",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "add_list_subpartition",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AddListSubpartition",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "add_range_subpartition",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AddRangeSubpartition",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "coalesce_table_subpartition",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//CoalesceTableSubpartition",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "indexing_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//OnOrOff",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "local_indexes",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//LocalIndexes",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partition_attributes",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PartitionAttributes",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partition_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "read_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//OpenStatus",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "values",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ActionValues",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterIotClauses",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AddOverflowClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "overflow_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SegmentAttributesClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partition_clauses",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SegmentAttributesClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterOverflowClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AlterIotClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "add_overflow_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AddOverflowClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "allocate_extent_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AllocateExtentClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "deallocate_unused_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DeallocateUnusedClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "segment_attributes_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SegmentAttributesClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "shrink_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ShrinkClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "IndexOrgOverflowClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "including_column_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "overflow_segment_attributes_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SegmentAttributesClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "MappingTableClause",
      eLiterals: [
        {
          name: "MAPPING_TABLE",
        },
        {
          name: "NO_MAPPING",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "IndexOrgTableClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AlterIotClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "index_org_overflow_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IndexOrgOverflowClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "key_compression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//KeyCompression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "mapping_table_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//MappingTableClause",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "pct_threshold",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntLiteral",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CoalesceIotClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AlterIotClauses",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterMappingTableClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AlterIotClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "allocate_extent_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AllocateExtentClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "deallocate_unused_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DeallocateUnusedClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ModifyTablePartitionHash",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ModifyTablePartition",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "alter_mapping_table_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AlterMappingTableClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "coalesce_table_subpartition",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//CoalesceTableSubpartition",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "indexing_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//OnOrOff",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "local_indexes",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//LocalIndexes",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partition_attributes",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PartitionAttributes",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partition_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "read_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//OpenStatus",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ModifyTablePartitionRange",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ModifyTablePartition",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "add_hash_subpartition",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AddHashSubpartition",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "add_list_subpartition",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AddListSubpartition",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "add_range_subpartition",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AddRangeSubpartition",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "alter_mapping_table_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AlterMappingTableClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "coalesce_table_subpartition",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//CoalesceTableSubpartition",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "indexing_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//OnOrOff",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "local_indexes",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//LocalIndexes",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partition_attributes",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PartitionAttributes",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partition_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "read_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//OpenStatus",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ActionValues",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ModifyTablePartition",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "action",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//Actions",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "values",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "RebuildFreepools",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ModifyLobParameter",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "allocate_extent_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AllocateExtentClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "deallocate_unused_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DeallocateUnusedClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "encryption",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Encryption",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "freepools",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Freepools",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "lob_compression_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//LobCompressionClause",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "lob_deduplicate_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//LobDeduplicateClause",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "lob_retention_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//LobRetentionClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "modify_lob_parameter_cache",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//LobParameterCache",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "pctversion",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Pctversion",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "rebuild_freepools",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//RebuildFreepools",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "shrink_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ShrinkClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "storage_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StorageClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ModifyLobParameters",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parameters",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ModifyLobParameter",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ModifyTableSubpartitionLobElement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "lob_item",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "modify_lob_parameters",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ModifyLobParameters",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "varray_item",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//VarrayItem",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ModifyTableSubpartitionLob",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ModifyTableSubpartitionLobElement",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ModifyTableSubpartition",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "allocate_extent_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AllocateExtentClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "deallocate_unused_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DeallocateUnusedClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "indexing_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//OnOrOff",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "local_indexes",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//LocalIndexes",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "modify_table_subpartition_lob",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ModifyTableSubpartitionLob",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "read_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//OpenStatus",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "shrink_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ShrinkClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "subpartition_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterTablePartitioning",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "alter_automatic_partitioning",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AlterAutomaticPartitioning",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "modify_index_default_attrs",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ModifyIndexDefaultAttrs",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "modify_table_partition",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ModifyTablePartition",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "modify_table_subpartition",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ModifyTableSubpartition",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "subpartition_template",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SubpartitionTemplate",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterTablePropertiesClauses",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "alter_iot_clauses",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AlterIotClauses",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "clauses",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AlterTablePropertiesClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "AlterTablePropertiesOptions",
      eLiterals: [
        {
          name: "RENAME",
        },
        {
          name: "READ_ONLY",
          value: 1,
        },
        {
          name: "READ_WRITE",
          value: 2,
        },
        {
          name: "SHRINK",
          value: 3,
        },
        {
          name: "REKEY",
          value: 4,
        },
        {
          name: "CLAUSES",
          value: 5,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterTableProperties",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "alter_table_properties_clauses",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AlterTablePropertiesClauses",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//AlterTablePropertiesOptions",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "rekey_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "shrink_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ShrinkClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tableview_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableviewName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ColumnClauses",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ModifyLobStorageClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ColumnClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "lob_item",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "modify_lob_parameters",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ModifyLobParameters",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ModifyCollectionRetrieval",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ColumnClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "collection_item",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableviewName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "return_as",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//LocatorOrValue",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "RenameColumnClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ColumnClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "new_column_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "old_column_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AddModifyDropColumnClauses",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ColumnClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "add_column_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AddColumnClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "drop_column_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DropColumnClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "modify_column_clauses",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ModifyColumnClauses",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ConstraintClauses",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DropConstraintClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ConstraintClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "cascade",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "drop_what",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TargetElement",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "keep_or_drop",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//KeepOrDrop",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DropConstraintClauses",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ConstraintClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "drop_clauses",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DropConstraintClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "RenameConstraintClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ConstraintClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "new_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "old_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ModifyConstraintClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ConstraintClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "cascade",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "constraint_state",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ConstraintState",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "element",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TargetElement",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AddConstraintClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//ConstraintClauses",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "out_of_line_constraint",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//OutOfLineConstraint",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "out_of_line_ref_constraint",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//OutOfLineRefConstraint",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "MoveTableClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "index_org_table_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IndexOrgTableClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "lob_storage_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//LobStorageClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "online",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parallel_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ParallelClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "segment_attributes_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SegmentAttributesClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "table_compression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//TableCompression",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "varray_col_properties",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//VarrayColProperties",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "AlterTableOptions",
      eLiterals: [
        {
          name: "ENABLE_TABLE_LOCK",
        },
        {
          name: "DISABLE_TABLE_LOCK",
          value: 1,
        },
        {
          name: "ENABLE_ALL_TRIGGERS",
          value: 2,
        },
        {
          name: "DISABLE_ALL_TRIGGERS",
          value: 3,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterTable",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "alter_external_table",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AlterExternalTable",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "alter_table_partitioning",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AlterTablePartitioning",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "alter_table_properties",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AlterTableProperties",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_clauses",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ColumnClauses",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "constraint_clauses",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ConstraintClauses",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "enable_disable_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//EnableDisableClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "move_table_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//MoveTableClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "options",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//AlterTableOptions",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tableview_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableviewName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "AlterSessionValue",
      eLiterals: [
        {
          name: "ADVISE_COMMIT",
        },
        {
          name: "ADVISE_ROLLBACK",
          value: 1,
        },
        {
          name: "ADVISE_NOTHING",
          value: 2,
        },
        {
          name: "CLOSE_DATABASE_LINK",
          value: 3,
        },
        {
          name: "ENABLE_COMMIT_IN_PROCEDURE",
          value: 4,
        },
        {
          name: "DISABLE_COMMIT_IN_PROCEDURE",
          value: 5,
        },
        {
          name: "ENABLE_GUARD",
          value: 6,
        },
        {
          name: "DISABLE_GUARD",
          value: 7,
        },
        {
          name: "ENABLE_PARALLEL",
          value: 8,
        },
        {
          name: "DISABLE_PARALLEL",
          value: 9,
        },
        {
          name: "FORCE_PARALLEL",
          value: 10,
        },
        {
          name: "SET",
          value: 11,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterSession",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "value",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//AlterSessionValue",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "ParallelSession",
      eLiterals: [
        {
          name: "DML",
        },
        {
          name: "DDL",
          value: 1,
        },
        {
          name: "QUERY",
          value: 2,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ParallelAlterSession",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AlterSession",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "value_parallel",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "what_parallel",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ParallelSession",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SetAlterSession",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AlterSession",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parameter_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parameter_value",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CloseDatabaseAlterSession",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AlterSession",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parameter_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "BasicAlterSession",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//AlterSession",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterSequence",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "sequence_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "sequence_spec",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SequenceSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterProcedure",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "compiler_parameters_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//CompilerParametersClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "debug",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "procedure_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "reuse",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterPackage",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "compiler_parameters_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//CompilerParametersClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "debug",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "package_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "reuse_settings",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "scope",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ConstructScope",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AddMvLogColumnClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "MoveMvLogClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parallel_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ParallelClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "segment_attributes_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SegmentAttributesClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "MvLogAugmentation",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_name",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "mv_log_element",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//WithClausesElement",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "new_values_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//NewValuesClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterMaterializedViewLog",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "add_mv_log_column_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AddMvLogColumnClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "allocate_extent_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AllocateExtentClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "alter_table_partitioning",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AlterTablePartitioning",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "cache_or_nocache",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//CacheOption",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "force",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "logging_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//LoggingClauseValue",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "move_mv_log_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//MoveMvLogClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "mv_log_augmentation",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//MvLogAugmentation",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "mv_log_purge_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//MvLogPurgeClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parallel_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ParallelClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "physical_attributes_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PhysicalAttributesClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "shrink_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ShrinkClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tableview_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableviewName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "RefreshValue",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "RefreshOptions",
      eLiterals: [
        {
          name: "FAST",
        },
        {
          name: "COMPLETE",
          value: 1,
        },
        {
          name: "FORCE",
          value: 2,
        },
        {
          name: "ON_DEMAND",
          value: 3,
        },
        {
          name: "ON_COMMIT",
          value: 4,
        },
        {
          name: "START_WITH",
          value: 5,
        },
        {
          name: "NEXT",
          value: 6,
        },
        {
          name: "WITH_PRIMARY_KEY",
          value: 7,
        },
        {
          name: "USING_DEFAULT_MASTER_ROLLBACK_SEGMENT",
          value: 8,
        },
        {
          name: "USING_MASTER_ROLLBACK_SEGMENT",
          value: 9,
        },
        {
          name: "USING_ENFORCED_CONSTRAINTS",
          value: 10,
        },
        {
          name: "USING_TRUSTED_CONSTRAINTS",
          value: 11,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ExpressionRefreshValue",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//RefreshValue",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//RefreshOptions",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "RollbackSegmentRefreshValue",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//RefreshValue",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//RefreshOptions",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "rollback_segment",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "BasicRefreshValue",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//RefreshValue",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//RefreshOptions",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterMvRefresh",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "options",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//RefreshValue",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterMvOption",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "alter_mv_refresh",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AlterMvRefresh",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ModifyMvColumnClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "encryption",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Encryption",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "AlterMaterializedViewOptions",
      eLiterals: [
        {
          name: "ENABLE_QUERY_REWRITE",
        },
        {
          name: "DISABLE_QUERY_REWRITE",
          value: 1,
        },
        {
          name: "COMPILE",
          value: 2,
        },
        {
          name: "CONSIDER_FRESH",
          value: 3,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterMaterializedView",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "allocate_extent_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AllocateExtentClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "alter_iot_clauses",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AlterIotClauses",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "alter_mv_option1",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AlterMvOption",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "alter_table_partitioning",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AlterTablePartitioning",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "cache_or_nocache",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//CacheOption",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "deallocate_unused_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DeallocateUnusedClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "lob_storage_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//LobStorageClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "logging_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//LoggingClauseValue",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "modify_lob_storage_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ModifyLobStorageClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "modify_mv_column_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ModifyMvColumnClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "modify_scoped_table_ref_constraint",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ScopeOutOfLineRefConstraint",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//AlterMaterializedViewOptions",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parallel_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ParallelClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "physical_attributes_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PhysicalAttributesClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "shrink_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ShrinkClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "table_compression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//TableCompression",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tableview_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableviewName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "LibraryDebug",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "LibraryEditionable",
      eLiterals: [
        {
          name: "EDITIONABLE",
        },
        {
          name: "NON_EDITIONABLE",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "LibraryName",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "regular_id",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterLibrary",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "compile",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "compiler_parameters_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//CompilerParametersClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "library_debug",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//LibraryDebug",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "library_editionable",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//LibraryEditionable",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "library_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//LibraryName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "reuse",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterFunction",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "compiler_parameters_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//CompilerParametersClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "debug",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "function_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "reuse_settings",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "InsertStatement",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "InsertIntoClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "general_table_ref",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//GeneralTableRef",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "paren_column_list",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SingleTableInsert",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//InsertStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "error_logging_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ErrorLoggingClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "insert_into_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//InsertIntoClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "select_statement",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SelectStatement",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "static_returning_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StaticReturningClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "values_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ValuesClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "MultiTableElement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "error_logging_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ErrorLoggingClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "insert_into_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//InsertIntoClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "values_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ValuesClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ConditionalInsertElsePart",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "multi_table_element",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//MultiTableElement",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ConditionalInsertWhenPart",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "condition",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "multi_table_element",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//MultiTableElement",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ConditionalInsertClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "else_part",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ConditionalInsertElsePart",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "target",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//PositionTarget",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "when_parts",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ConditionalInsertWhenPart",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "MultiTableInsert",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//InsertStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "conditional_insert_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ConditionalInsertClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "multi_table_elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//MultiTableElement",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "select_statement",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SelectStatement",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "RenameObject",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "from_object_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "to_object_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CommentOnTable",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "is_string",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tableview_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableviewName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CommentOnColumn",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "is_string",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//StringLiteral",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "FirstOrNext",
      eLiterals: [
        {
          name: "FIRST",
        },
        {
          name: "NEXT",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "OnlyOrWithTies",
      eLiterals: [
        {
          name: "ONLY",
        },
        {
          name: "WITH_TIES",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "RowOrRows",
      eLiterals: [
        {
          name: "ROW",
        },
        {
          name: "ROWS",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "FetchClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "first_or_next",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//FirstOrNext",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "only_or_with_ties",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//OnlyOrWithTies",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "percent_keyword",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "row_or_rows",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//RowOrRows",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "ForUpdateOptionValue",
      eLiterals: [
        {
          name: "SKIP_LOCKED",
        },
        {
          name: "WAIT",
          value: 1,
        },
        {
          name: "NO_WAIT",
          value: 2,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ForUpdateOptions",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ForUpdateOptionValue",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ForUpdateClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_list",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "for_update_options",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ForUpdateOptions",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "OffsetClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "row_or_rows",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//RowOrRows",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SelectStatement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//UnitStatement",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "fetch_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//FetchClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "for_update_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ForUpdateClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "offset_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//OffsetClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "order_by_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//OrderByClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "query",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Query",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DmlTableExpressionClauseSelect",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//DmlTableExpressionClause",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "select_statement",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SelectStatement",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "subquery_restriction_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SubqueryRestrictionClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DmlTableExpressionClauseCollection",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//DmlTableExpressionClause",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "table_collection_expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableCollectionExpression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TableRefAuxInternalExpression",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//TableRefAuxInternal",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "dml_table_expression_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//DmlTableExpressionClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "only",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "pivot_or_unpivot_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PivotOrUnpivotClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TableRefAux",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "flashback_query_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//FlashbackQueryClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "table_alias",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "table_ref_aux_internal",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableRefAuxInternal",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "JoinClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "cross_or_natural",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//CrossOrNatural",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "inner_or_outer",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//InnerOrOuter",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "join_on",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "join_or_apply",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//JoinOrApply",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "join_using",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//List",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "outer_join_type",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//OuterJoinType",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "query_partition_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//QueryPartitionClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "table_ref_aux",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableRefAux",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TableRef",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "join_clause",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//JoinClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "pivot_or_unpivot_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//PivotOrUnpivotClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "table_ref_aux",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableRefAux",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "FromClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "tablerefs",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableRef",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "GroupByElements",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "CubeOrRollup",
      eLiterals: [
        {
          name: "CUBE",
        },
        {
          name: "ROLLUP",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "GroupingSetsElements",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "GroupingSetsElementsExpressions",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//GroupingSetsElements",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expressions",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "GroupingSetsElementsRollup",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//GroupingSetsElements",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "rollup_cube_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//RollupCubeClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "RollupCubeClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "cube_or_rollup",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//CubeOrRollup",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "grouping_sets_elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//GroupingSetsElements",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "GroupByElementsRollup",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//GroupByElements",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "rollup_cube_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//RollupCubeClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "GroupingSetsClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "grouping_sets_elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//GroupingSetsElements",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "GroupByElementsGroupingSets",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//GroupByElements",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "grouping_sets_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//GroupingSetsClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "GroupByElementsExpression",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//GroupByElements",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "HavingClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "condition",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "GroupByClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "group_by_elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//GroupByElements",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "having_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//HavingClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "HierarchicalQueryClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "condition",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "no_cycle",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "start_condition",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "CellReferenceOptions",
      eLiterals: [
        {
          name: "IGNORE_NAV",
        },
        {
          name: "KEEP_NAV",
          value: 1,
        },
        {
          name: "UNIQUE_DIMENSION",
          value: 2,
        },
        {
          name: "UNIQUE_SINGLE_REFERENCE",
          value: 3,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ModelColumn",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "column_alias",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "query_block",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//QueryBlock",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ModelColumnClauses",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "dimensions_columns",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ModelColumn",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "measures_columns",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ModelColumn",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "partition_columns",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ModelColumn",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ModelIterateClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "iterate",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "until",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "UpdateOrUpsert",
      eLiterals: [
        {
          name: "UPDATE",
        },
        {
          name: "UPSERT",
          value: 1,
        },
        {
          name: "UPSERT_ALL",
          value: 2,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ModelRulesElement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "cell_assignment",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ModelExpression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "option",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//UpdateOrUpsert",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "order_by_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//OrderByClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "Order",
      eLiterals: [
        {
          name: "AUTOMATIC",
        },
        {
          name: "SEQUENTIAL",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ModelRulesClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "model_iterate_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ModelIterateClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "model_rules_element",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ModelRulesElement",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "order",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//Order",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "rules_option",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//UpdateOrUpsert",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "MainModel",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "cell_reference_options",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//CellReferenceOptions",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "main_model_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "model_column_clauses",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ModelColumnClauses",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "model_rules_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ModelRulesClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ReferenceModel",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "cell_reference_options",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//CellReferenceOptions",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "model_column_clauses",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ModelColumnClauses",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "reference_model_name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "subquery",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Subquery",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "ReturnRowsClause",
      eLiterals: [
        {
          name: "RETURN_UPDATED",
        },
        {
          name: "RETURN_ALL",
          value: 1,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ModelClause",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "cell_reference_options",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//CellReferenceOptions",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "main_model",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//MainModel",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "reference_model",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ReferenceModel",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "return_rows_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ReturnRowsClause",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "SelectModifierType",
      eLiterals: [
        {
          name: "UNIQUE",
        },
        {
          name: "ALL",
          value: 1,
        },
        {
          name: "DISTINCT",
          value: 2,
        },
        {
          name: "NONE",
          value: 3,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "QueryBlock",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "from_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//FromClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "group_by_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//GroupByClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "hierarchical_query_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//HierarchicalQueryClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "into_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//IntoClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "model_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ModelClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "modifier",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//SelectModifierType",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "order_by_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//OrderByClause",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "selected",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SelectElement",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "where_clause",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//WhereClause",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SubqueryElements",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "behavior",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//SubqueryBehavior",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "block",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//QueryBlock",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "subquery",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Subquery",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "Subquery",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Expression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SubqueryElements",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ParenExpression",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Expression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expressions",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "OuterJoin",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Expression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "table",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "GeneralElement",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Expression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "elements",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ElementId",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Expression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "arguments",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Arguments",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "charset",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "link",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "names",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "BindVariable",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Expression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "bind",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//string",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "indexing",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "indicator",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//string",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "isInt",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "parts",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "preInt",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//BigInteger",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TableElementExpression",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Expression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "alias",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "container",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TableviewName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AllElementExpression",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Expression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "name",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//string",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TableCollectionExpression",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Expression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "outer_join_sign",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "subquery",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Subquery",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "table",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DecLiteral",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Expression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "m_modifier",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "value",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//BigDecimal",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "Negated",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Expression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "value",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "IntLiteral",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Expression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "value",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//BigInteger",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ExtendedSqlId",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Expression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "id",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//string",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "StringFormat",
      eLiterals: [
        {
          name: "STANDARD",
        },
        {
          name: "QUOTED",
          value: 1,
        },
        {
          name: "NATIONAL",
          value: 2,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "StringLiteral",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Expression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "format",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//StringFormat",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "value",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//string",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "DateLiteral",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Expression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "value",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//string",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "Interval",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Expression",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ConstantInterval",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Interval",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "first_value",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "from_time",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//TimeType",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "interval_time",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "second_value",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "to_second_value",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "to_time",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//TimeType",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "FromToInterval",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Interval",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "from",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "time_from",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//TimeType",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "time_to",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//TimeType",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "to",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AtInterval",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Interval",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "at",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "time",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//TimeType",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "Timestamp",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Expression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "at_time_zone",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "time",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "BooleanLiteral",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Expression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "value",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "Constant",
      eLiterals: [
        {
          name: "DEFAULT",
        },
        {
          name: "MINVALUE",
          value: 1,
        },
        {
          name: "MAXVALUE",
          value: 2,
        },
        {
          name: "DBTIMEZONE",
          value: 3,
        },
        {
          name: "SESSIONTIMEZONE",
          value: 4,
        },
        {
          name: "NULL",
          value: 5,
        },
        {
          name: "ANY",
          value: 6,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ConstantExpr",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Expression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "constant",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//Constant",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CaseExpression",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Expression",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CaseExpressionSimple",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//CaseExpression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "simple_case_statement",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SimpleCaseStatement",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CaseExpressionSearched",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//CaseExpression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "searched_case_statement",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//SearchedCaseStatement",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "UnaryOperatorType",
      eLiterals: [
        {
          name: "NEW",
        },
        {
          name: "PRIOR",
          value: 1,
        },
        {
          name: "CONNECT_BY_ROOT",
          value: 2,
        },
        {
          name: "DISTINCT",
          value: 3,
        },
        {
          name: "ADDITION",
          value: 4,
        },
        {
          name: "SUBTRACTION",
          value: 5,
        },
        {
          name: "ALL",
          value: 6,
        },
        {
          name: "SOME",
          value: 7,
        },
        {
          name: "EXISTS",
          value: 8,
        },
        {
          name: "ANY",
          value: 9,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "UnaryExpression",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Expression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "operator",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//UnaryOperatorType",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "value",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ModelExpression",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Expression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "element",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "interval_expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "BetweenElements",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "left",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "right",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CompoundExpression",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Expression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "between_elements",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//BetweenElements",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "escape_concatenation",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "in_elements",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "like_concatenation",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "like_type",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//string",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "main_concatenation",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "negated",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "MultisetType",
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "multiset",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//string",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "of",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "MultisetExpression",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Expression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "concatenation",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "multiset_type",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//MultisetType",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "LogicalOperationType",
      eLiterals: [
        {
          name: "NULL",
        },
        {
          name: "NAN",
          value: 1,
        },
        {
          name: "PRESENT",
          value: 2,
        },
        {
          name: "INFINITE",
          value: 3,
        },
        {
          name: "A_SET",
          value: 4,
        },
        {
          name: "EMPTY",
          value: 5,
        },
        {
          name: "TYPE",
          value: 6,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "LogicalOperation",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "negated",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "only",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "operation",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//LogicalOperationType",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_spec",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "UnaryLogicalExpression",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Expression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "expression",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "negated",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//boolean",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "operations",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//LogicalOperation",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CursorExpression",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Expression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "query",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Subquery",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "BinaryExpression",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Expression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "left",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "right",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//Expression",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "ConcatenationOperatorType",
      eLiterals: [
        {
          name: "CONCATENATION",
        },
        {
          name: "MULTIPLICATION",
          value: 1,
        },
        {
          name: "DIVISION",
          value: 2,
        },
        {
          name: "ADDITION",
          value: 3,
        },
        {
          name: "SUBTRACTION",
          value: 4,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ConcatenationExpression",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//BinaryExpression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "op",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//ConcatenationOperatorType",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
      name: "RelationalOperatorType",
      eLiterals: [
        {
          name: "EQUALITY",
        },
        {
          name: "INEQUALITY",
          value: 1,
        },
        {
          name: "LESS",
          value: 2,
        },
        {
          name: "LESS_EQUAL",
          value: 3,
        },
        {
          name: "GREATER",
          value: 4,
        },
        {
          name: "GREATER_EQUAL",
          value: 5,
        },
        {
          name: "MULTISET",
          value: 6,
        },
        {
          name: "MULTISET_INTERSECT",
          value: 7,
        },
        {
          name: "MULTISET_EXCEPT",
          value: 8,
        },
        {
          name: "MULTISET_UNION",
          value: 9,
        },
        {
          name: "MULTISET_UNION_ALL",
          value: 10,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "RelationalExpression",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//BinaryExpression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "operator",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//RelationalOperatorType",
          },
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "LogicalExpression",
      abstract: true,
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//BinaryExpression",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ModExpression",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//LogicalExpression",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "OrExpression",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//LogicalExpression",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AndExpression",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//LogicalExpression",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "TempExpression",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Expression",
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "ElementName",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "//Expression",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "charset",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "names",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ExtendedSqlId",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AttributeDefinition",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "name",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//ElementName",
          },
          containment: true,
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "type_spec",
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//TypeSpec",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "AlterAttribute",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "action",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EEnum",
            $ref: "//Actions",
          },
        },
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "attributes",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "//AttributeDefinition",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "CompilationUnit",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EReference",
          name: "script",
          upperBound: -1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
            $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
          },
          containment: true,
        },
      ],
    },
    {
      eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
      name: "SqlPlusCommand",
      eSuperTypes: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EClass",
          $ref: "https://strumenta.com/kolasu/v1#//ASTNode",
        },
      ],
      eStructuralFeatures: [
        {
          eClass: "http://www.eclipse.org/emf/2002/Ecore#//EAttribute",
          name: "text",
          lowerBound: 1,
          eType: {
            eClass: "http://www.eclipse.org/emf/2002/Ecore#//EDataType",
            $ref: "https://strumenta.com/kolasu/v1#//string",
          },
        },
      ],
    },
  ],
};
