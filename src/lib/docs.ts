export default { 
  DOCS_URL : 'https://languages-documentation-39d33898f50f.herokuapp.com', 
  DOCS_MAP : { 
    java: 'kolasu-java-langmodule',
    python: 'kolasu-python-langmodule',
    sas: 'sas-parser',
  }
}  