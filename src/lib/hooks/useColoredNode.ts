import { useEffect, useState } from "react";
import getSelectionPath from "../utils/getSelectionPath";

function useSelectionColors(args) {
  const { node, path, level, selection, isSelected } = args ?? {};
  const [colorClass, setColorClass] = useState("");

  useEffect(() => {
    // if no selection applied, all grey
    if (!selection) {
      setColorClass("");
      return;
    }
    if (isSelected) {
      setColorClass("sel-main");
      return;
    }

    const color = `sel-level-${(level + 1) % 10}`;
    const selPath = getSelectionPath(selection);
    const isInSelectionPath =
      selPath.substring(0, path.length) === path && selPath !== path;

    // path is falsy on root node
    if (isInSelectionPath && path) {
      setColorClass(color);
      return;
    }
    setColorClass("");
  }, [node, selection, isSelected, path, level]);

  return colorClass;
}

export default useSelectionColors;
