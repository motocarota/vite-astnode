import { useEffect } from 'react';

function useScrollToNode(args) {
  const {
    nodeRef,
    selection,
    isSelected,
  } = args ?? {};

  useEffect(
    () => {
      if (isSelected && selection?.from === 'code') {
        nodeRef?.current?.scrollIntoView();
        // nodeRef?.current?.scrollIntoView({ behavior: 'smooth' });
      }
    },
    [nodeRef, selection, isSelected],
  );
}

export default useScrollToNode;
