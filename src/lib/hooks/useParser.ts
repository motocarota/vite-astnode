import { useContext, useEffect, useState } from "react";
import { SelectionContext } from "../contexts/SelectionProvider";
import { parseParserExample } from "../utils/ast.strumenta";
import getFields from "../utils/getFields";

function useParser(args) {
  const { ast, langId, meta } = args ?? {};
  const [tree, setTree] = useState();
  const [rootNode, setRootNode] = useState(null);
  const [error, setError] = useState();
  const [parsing, setParsing] = useState(false);

  const { storeTree } = useContext(SelectionContext);

  // stores tree into selection context
  useEffect(() => {
    storeTree({ s: rootNode, t: null, isTranspiler: false });
  }, [rootNode, storeTree]);

  useEffect(() => {
    setParsing(true);
    setError(null);

    let t = null;
    try {
      const trace = parseParserExample({
        ast,
        meta,
        langId,
      });
      const node = trace?.rootNode;
      setRootNode(node);

      t = getFields({
        node,
        level: 0,
        isTranspiler: false,
        from: "parse",
      });
      setParsing(false);
    } catch (err) {
      console.error(err);
      setError(err.message);
      setParsing(false);
    }
    setTree(t);
  }, [ast, langId, meta]);

  return [tree, rootNode, error, parsing];
}

export default useParser;
