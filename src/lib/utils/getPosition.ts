import _isFunction from "lodash/isFunction";

function getPosition(node) {
  if (!_isFunction(node?.getPosition)) {
    return {};
  }
  const position = node?.getPosition();
  if (!position) {
    return {};
  }
  return position;
}

export default getPosition;
