import style from "./NodeType.module.css";

function NodeType(props) {
  const { type } = props;

  if (!type) {
    return null;
  }
  return (
    <span className={style.nodetype}>
      {type}
    </span>
  );
}

export default NodeType;
