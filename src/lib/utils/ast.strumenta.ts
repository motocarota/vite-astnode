import _toString from "lodash/toString";
import _map from "lodash/map";
import _filter from "lodash/filter";
import _includes from "lodash/includes";
import {
  ParserTraceLoader,
  TranspilationTraceLoader,
} from "@strumenta/tylasu/interop/strumenta-playground";
import isNode from "./isNode";

function presentAttrChild(node) {
  const key = node?.eContainingFeature?.get("name");

  if (node.get("many")) {
    return {
      key,
      value: "[many]",
      // TODO placeholder: andrebbe fatto il toString della lista di valori
    };
  }
  const value = node?.get(key) || "[undefined]";

  return {
    key,
    value: _toString(value),
  };
}

export function parseTranspilerExample({ ast, langId, tLangId, meta, tMeta }) {
  if (!ast || !meta || !tMeta) {
    return null;
  }
  const loader = new TranspilationTraceLoader(
    {
      name: langId,
      uri: `file://tests/data/playground/${langId}-metamodels.json`,
      metamodel: meta,
    },
    {
      name: tLangId,
      uri: `file://tests/data/playground/${tLangId}-metamodels.json`,
      metamodel: tMeta,
    }
  );

  return loader.loadTranspilationTrace(ast);
}

export function parseParserExample({ ast, meta, langId }) {
  if (!ast || !meta) {
    return null;
  }
  const loader = new ParserTraceLoader({
    name: langId,
    uri: `file://tests/data/playground/${langId}examples/metamodel.json`,
    metamodel: meta,
  });

  return loader.loadParserTrace(ast, langId);
}

function isChildAttribute(child) {
  return (
    !isNode(child) &&
    child?.eContainingFeature?.get("name") &&
    !_includes(
      ["Origin", "Destination", "Position"],
      child?.eContainingFeature?.get("name")
    )
  );
}

export function getChildAttributes(childrens) {
  return _map(_filter(childrens, isChildAttribute), presentAttrChild);
}
