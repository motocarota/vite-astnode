import { Position, findByPosition } from "@strumenta/tylasu";
import { TraceNode } from "@strumenta/tylasu/interop/strumenta-playground";
import { extractPosition } from "./positions";

interface In {
  position: Position;
  tree: TraceNode;
}

function getNodeFromPoint({ position, tree }: In): TraceNode {
  if (!position || !tree) {
    return null;
  }
  const pos = extractPosition(position);
  const node = findByPosition(tree, pos) as TraceNode;

  return node;
}

export default getNodeFromPoint;
