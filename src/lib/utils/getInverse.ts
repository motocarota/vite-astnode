import { Position } from "@strumenta/tylasu";
import {
  SourceNode,
  TargetNode,
} from "@strumenta/tylasu/interop/strumenta-playground";
import _get from "lodash/get";

export interface UglyNodeTypeWorkaround {
  getSourceNode?: () => SourceNode;
  getDestinationNodes?: () => TargetNode;
  getDestinationNode?: () => TargetNode;
}

interface In {
  // NOTE this is a workaround, should use the correct type
  node?: UglyNodeTypeWorkaround | null;
  isTranspiler: boolean;
  isTarget: boolean;
}

interface Out {
  inverseKey: string | null;
  inverseFile: string | null;
  inverseNode: TargetNode | SourceNode | null;
  inversePosition: Position | null;
}

// returns the opposite of the current node
// from source to target use sourceNode.getDestinationNode().destination
// from target to source use targetNode.getSourceNode().position
// includes also the key for AST node checks
function getInverse({ node, isTranspiler, isTarget }: In): Out {
  if (!node || !isTranspiler) {
    return {
      inversePosition: null,
      inverseKey: null,
      inverseNode: null,
      inverseFile: null,
    };
  }

  // eslint-disable-next-line no-nested-ternary
  const getter = isTarget
    ? node?.getSourceNode
    : () => node?.getDestinationNodes()[0] ?? null;

  let res;
  let ik = null;
  let ip = null;

  try {
    res = getter.apply(node);
  } catch (e) {
    return {
      inversePosition: null,
      inverseKey: null,
      inverseNode: null,
      inverseFile: null,
    };
  }

  try {
    const title = res.getRole();
    const eClassName = res.getSimpleType();
    const path = res.getPathFromRoot().join();
    ik = `${path}.${eClassName}.${title}`;
  } catch (e) {
    // console.warn('error looking for inverse key');
  }

  try {
    ip = isTarget ? res?.getPosition() : res?.getDestination();
  } catch (e) {
    console.warn("error looking for inverse position");
  }

  return {
    inverseNode: res,
    inversePosition: ip,
    inverseKey: ik,
    inverseFile: _get(res, ["file", "path"], null),
  };
}

export default getInverse;
