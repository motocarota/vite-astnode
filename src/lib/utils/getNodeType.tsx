function getNodeType(node): string | null {
  if (node.isStatementNode && node?.isStatementNode()) {
    return "Statement";
  }
  if (node.isExpressionNode && node?.isExpressionNode()) {
    return "Expression";
  }
  if (node.isDeclarationNode && node?.isDeclarationNode()) {
    return "Declaration";
  }
  return null;
}

export default getNodeType;
