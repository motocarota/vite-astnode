import _isEmpty from "lodash/isEmpty";

export default function isNode(ast) {
  if (!ast) {
    return false;
  }
  const allSuperTypes = ast?.eo?.eClass?.get("eAllSuperTypes") || [];
  const astNodes = allSuperTypes.filter((st) => st.get("name") === "ASTNode");

  return !_isEmpty(astNodes);
}
