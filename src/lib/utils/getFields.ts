import { TraceNode } from "@strumenta/tylasu/interop/strumenta-playground";
import getNodeType from "./getNodeType";
import isNode from "./isNode";
import getInverse, { UglyNodeTypeWorkaround } from "./getInverse";
import getPosition from "./getPosition";
import getAttributes from "./getAttributes";
import getChildrens from "./getChildrens";
import WrappedNode from "../../types/WrappedNode.type";
import From from "../../types/From.type";

interface Props {
  node: TraceNode;
  level?: number;
  recursive?: boolean;
  isTranspiler: boolean;
  isTarget?: boolean;
  hasMultipleFiles?: boolean;
  from: From;
}

function getFields(args: Props): WrappedNode {
  const {
    node,
    level = 0,
    recursive = true,
    isTranspiler = false,
    isTarget,
    hasMultipleFiles = false,
    from,
  } = args ?? {};

  if (!isNode(node)) {
    return null;
  }
  const title = node.getRole();
  const eClassName = node.getSimpleType();
  const qualifiedName = node.getType();
  const type = getNodeType(node);
  const path = node.getPathFromRoot().join();
  const key = `${path}.${eClassName}.${title}`;
  const attributes = getAttributes(node);
  const position = getPosition(node);
  const { inverseKey, inverseFile, inverseNode, inversePosition } = getInverse({
    node: node as UglyNodeTypeWorkaround,
    isTranspiler,
    isTarget,
  });

  const childrens = recursive
    ? getChildrens(
        {
          node,
          level,
          recursive,
          isTranspiler,
          isTarget,
          hasMultipleFiles,
          from,
        },
        getFields
      )
    : [];

  return {
    key,
    title,
    path,
    type,
    eClassName,
    qualifiedName,
    attributes,
    childrens,
    position,
    inverseKey,
    inverseFile,
    inverseNode,
    inversePosition,
    original: node,
    isTranspiler,
    from,
    hasMultipleFiles,
  };
}

export default getFields;
