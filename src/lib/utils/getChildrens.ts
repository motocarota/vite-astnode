import _map from "lodash/map";
import _filter from "lodash/filter";
import { TraceNode } from "@strumenta/tylasu/interop/strumenta-playground";
import WrappedNode from "../../types/WrappedNode.type";
import From from "../../types/From.type";

interface Props {
  node: TraceNode;
  level?: number;
  recursive?: boolean;
  isTranspiler: boolean;
  isTarget: boolean;
  hasMultipleFiles: boolean;
  from: From;
}

function getChildrens(args: Props, func: (obj: unknown) => WrappedNode) {
  const { node, level = 0 } = args ?? {};
  // const children = node.getChildren();
  const { children } = node;
  if (!children) {
    return [];
  }

  return _filter(
    _map(children, (child) =>
      func({
        ...args,
        node: child,
        level: level + 1,
      })
    )
  );
}

export default getChildrens;
