import _get from "lodash/get";
import { pos } from "@strumenta/tylasu";

export function getPositionHash(position) {
  if (!position) {
    return "-";
  }
  const { startLineNumber, startColumn, endLineNumber, endColumn } = position;

  return `${startLineNumber}.${startColumn}.${endLineNumber}.${endColumn}
  `;
}

export function getPosition(position) {
  return {
    startLineNumber: _get(position, "start.line"),
    startColumn: _get(position, "start.column") + 1,
    endLineNumber: _get(position, "end.line"),
    endColumn: _get(position, "end.column"),
  };
}

export function extractPosition(arg) {
  if (arg.start && arg.end) {
    // no need to change anything
    return arg;
  }
  const { startLineNumber, startColumn, endLineNumber, endColumn } = arg;
  return pos(startLineNumber, startColumn, endLineNumber, endColumn);
}

export function createPosition(startLine, startColumn, endLine, endColumn) {
  return {
    start: {
      line: startLine || 1,
      column: (startColumn || 0) + 1,
    },
    end: {
      line: endLine || 1,
      column: (endColumn || 0) + 1,
    },
  };
}

export function positionBefore(p1, p2) {
  return {
    start: p1.start,
    end: p2.start,
  };
}

export function positionAfter(p1, p2) {
  return {
    start: p2.end,
    end: p1.end,
  };
}

export function isPointAfterOrOn(point, other) {
  if (point.line > other.line) {
    return true;
  }
  if (point.line < other.line) {
    return false;
  }
  return point.column >= other.column;
}

export function isPointBeforeOrOn(point, other) {
  if (point.line > other.line) {
    return false;
  }
  if (point.line < other.line) {
    return true;
  }
  return point.column <= other.column;
}

export function isPointWithinPosition(point, position) {
  return (
    isPointAfterOrOn(point, position.start) &&
    isPointBeforeOrOn(point, position.end)
  );
}

export function getNodePosition(eObject) {
  if (!eObject || !eObject.get) {
    return null;
  }
  const p = eObject.get("position");
  if (p == null) {
    return null;
  }
  const s = p.get("start");
  const e = p.get("end");
  return createPosition(
    s.get("line"),
    s.get("column"),
    e.get("line"),
    e.get("column")
  );
}
