import _map from 'lodash/map';
import _toString from 'lodash/toString';

function presentValue({ node, attribute }) {
  const key = attribute.get('name');

  if (attribute.get('many')) {
    return {
      key,
      value: '[many]',
      // TODO placeholder: andrebbe fatto il toString della lista di valori
    };
  }
  const value = node.getAttributes()[key] || '[undefined]';

  return {
    key,
    value: _toString(value),
  };
}

function getAttributes(node) {
  const attributes = node.eo.eClass.get('eAllAttributes');

  return _map(
    attributes,
    (attribute) => presentValue({ node, attribute }),
  );
}


export default getAttributes;