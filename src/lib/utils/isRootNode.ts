const isRootNode = (path) => {
  return path === "";
};

export default isRootNode;
