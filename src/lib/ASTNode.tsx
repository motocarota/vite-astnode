import { useCallback, useRef } from "react";
import _map from "lodash/map";
import _noop from "lodash/noop";
import ASTAttributes from "./ASTAttributes";
import Loader from "./Loader";
import useColoredNode from "./hooks/useColoredNode";
import useIsSelected from "./hooks/useIsSelected";
import useScrollToNode from "./hooks/useScrollToNode";
import useIsOpenNode from "./hooks/useIsOpenNode";
import isRootNode from "./utils/isRootNode";
import style from "./ASTNode.module.css";
import DocsLink from "./utils/DocsLink";
import NodeType from "./utils/NodeType";
import WrappedNode from "../types/WrappedNode.type";
import { SelectionCtx } from "../types/SelectionCtx.type";

interface Props {
  node: WrappedNode;
  level: number;
  langId: string;
  ctx: SelectionCtx;
  viewNode?: (i: { inverseFile: string }) => void;
  isTarget?: boolean;
  isTranspiler?: boolean;
  titleOverride?: string;
  hasDocs?: boolean;
  hasMultipleFiles?: boolean;
}

function ASTNode(props: Props) {
  const {
    node,
    level,
    langId,
    viewNode = _noop,
    isTarget,
    isTranspiler = false,
    titleOverride,
    ctx,
    hasDocs = false,
    hasMultipleFiles = false,
  } = props;

  const {
    key,
    path,
    title,
    eClassName,
    attributes,
    childrens,
    inverseFile,
    inversePosition,
    type,
    qualifiedName,
  } = node || {};

  const nodeRef = useRef(null);

  const { source, target, select } = ctx;
  const selection = isTarget ? target : source;

  // determines if the current node is selected
  const isSelected = useIsSelected({
    selection,
    nodeKey: key,
  });

  // scroll to current node if selected
  useScrollToNode({
    nodeRef,
    selection,
    isSelected,
  });

  // color nodes based on selection level
  const colorClass = useColoredNode({
    node,
    path,
    level,
    selection,
    isSelected,
  });

  // open/close nodes based on selection status
  const [isOpen, setOpen] = useIsOpenNode({
    node,
    path,
    level,
    selection,
    isSelected,
  });

  // determines if this is the root node
  const isRoot = isRootNode(path);

  const onSelect = useCallback(() => {
    select({
      node: isSelected || isRoot ? null : node,
      isTarget,
      isTranspiler,
      from: "AST",
    });
  }, [isRoot, isSelected, isTarget, isTranspiler, node, select]);

  const onViewNode = useCallback(() => {
    select({
      node,
      isTarget,
      isTranspiler,
      hasMultipleFiles,
      from: "AST",
    });
    viewNode({ inverseFile });
  }, [
    select,
    node,
    isTarget,
    isTranspiler,
    hasMultipleFiles,
    viewNode,
    inverseFile,
  ]);

  if (!node) {
    return <Loader loading />;
  }

  const toggleOpen = () => {
    setOpen((prev) => !prev);
  };

  const header = (
    <div
      className={`${colorClass} ${style.panelHeading}`}
      style={{
        display: "flex",
        justifyContent: "space-between",
      }}
      onClick={toggleOpen}
    >
      {isRoot ? (
        <div />
      ) : (
        <button type="button" className={style.selectButton} onClick={onSelect}>
          {isSelected ? "De-select" : "Select"}
        </button>
      )}
      <h6 className={style.title}>{titleOverride || title}</h6>
      <div style={{ cursor: "pointer" }}>{isOpen ? "-" : "+"}</div>
    </div>
  );

  if (!isOpen) {
    return (
      <div
        className={`pl-5 astnode ${isSelected ? "selection" : ""}`}
        ref={nodeRef}
      >
        <div className={style.header}>{header}</div>
      </div>
    );
  }

  const hasInverseNode = hasMultipleFiles ? !!inverseFile : !!inversePosition;

  return (
    <div
      key={key}
      className={`${style.selected} ${isSelected ? "selection" : ""}`}
      ref={nodeRef}
    >
      <div className={style.header}>
        {header}
        <div className={style.panelBlock}>
          <div>
            <NodeType type={type} />
            <strong>{eClassName}</strong>
            {hasDocs && (
              <DocsLink langId={langId} qualifiedName={qualifiedName} />
            )}
          </div>
          {hasInverseNode ? (
            <button
              type="button"
              className={style.selectButton}
              onClick={onViewNode}
            >
              {isTarget ? "Go to original code" : "Go to transpiled code"}
            </button>
          ) : (
            <div />
          )}
        </div>
        <ASTAttributes id={key} attributes={attributes} childrens={childrens} />
      </div>
      {_map(childrens, (child, i) => (
        <div key={`${key}[${i}]`}>
          <ASTNode
            node={child}
            level={level + 1}
            langId={langId}
            isTarget={isTarget}
            isTranspiler={isTranspiler}
            viewNode={viewNode}
            ctx={ctx}
            hasDocs={hasDocs}
            hasMultipleFiles={hasMultipleFiles}
          />
        </div>
      ))}
    </div>
  );
}

export default ASTNode;
